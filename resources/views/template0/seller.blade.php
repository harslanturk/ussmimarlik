@extends('layouts.template0.master')

@section('content')

<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Anasayfa</a></li>
						<li class="active">{{$pageReference[0]['title']}}</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>{{$pageReference[0]['title']}}</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<!--<ul class="nav nav-pills sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options='{"layoutMode": "fitRows", "filter": "*"}'>
			<li data-option-value="*" class="active"><a href="#">Show All</a></li>
			<li data-option-value=".websites"><a href="#">Websites</a></li>
			<li data-option-value=".logos"><a href="#">Logos</a></li>
			<li data-option-value=".brands"><a href="#">Brands</a></li>
			<li data-option-value=".medias"><a href="#">Medias</a></li>
		</ul>
	-->
		<hr>

		<div class="row">
		<style type="text/css">
			.sort-destination-loader.sort-destination-loader-showing {
				height:auto !important;
				overflow:visible;
			}
			.sort-destination-loader.sort-destination-loader-showing ul {
				overflow:visible;
			}
			.sort-destination {
				overflow:visible;
			}
			.thumb-info .thumb-info-title {
				bottom:0;
			}
			html.webkit .thumb-info .thumb-info-wrapper {
				height:150px;
			}
			html.webkit .thumb-info .thumb-info-wrapper img {
				top:25%;
			}
		</style>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">BASF</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/1482763830_BASF.png" style="height:130px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/yks.png" style="height:130px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/basf.png" style="height:130px; margin:auto;">
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
											Neden Basf?
										</a>
									</h4>
								</div>
								<div id="collapse1One" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											Tek takım, 112.000’den fazla oyuncu: BASF Grup çalışanları tüm dünya çapında çeşitli bölgelerde her gün birçok başarı hikayesine birlikte imza atıyor. Dünya çapındaki iş ortaklarımız BASF’nin benzersiz ürünlerine güven duyuyor çünkü her daim müşterilerimizin zorlu görevleri için akıllı çözümler bulmaya odaklanıyoruz. Yaratıcılığınızı, tutkunuzu ve bilginizi bizimle paylaşın ve gerçek bir global aktörün sunduğu fırsatlardan yararlanın.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Two">
											İnşaat Alanında Basf:
										</a>
									</h4>
								</div>
								<div id="collapse1Two" class="accordion-body collapse">
									<div class="panel-body">
										<p>
											BASF’de, sadece kimyasal üretmiyoruz – biz kimya yaratıyoruz – 50 yılı aşkın süredir, başarılı ve sürdürülebilir inşaat projeleri için, inşaat endüstrisindeki paydaşlarımızla yakın çalışıyoruz. BASF’nin kimyası ile yapılar, daha dayanıklı olabilir ve bakım için daha az kaynağa ihtiyaç duyar. Kimya aynı zamanda yapılarda, enerji verimliliği sağlayarak çevreyi korumaya yardımcı olur.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Three">
											150 Yıllık Bir Dev
										</a>
									</h4>
								</div>
								<div id="collapse1Three" class="accordion-body collapse">
									<div class="panel-body">
										<p>
											BASF, 2015 yılında 150. yılını kutluyor. Creator Space™ – BASF’nin yıldönümüne özel olarak hazırladığı birlikte yaratım programının adı. Yaklaşık iki yıldır bunun için hazırlanıyoruz. Dünya çapında bir çok çalışan ve şirket dışından uzmanlar programın içinde yer alıyor. Ödüllü belgesel film yapımcısı Thomas Grube düşünceleri, korkuları ve umutları hakkında bu kişilerle konuştu. Yandaki film Thomas Grube’nin 150. yıl serüveni hakkındakihislerini ve katılanların tutkusunu gösteriyor.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">ODE</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/1482763724_ode-logo.jpg" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/straforr.fw_-300x118.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/mebran.fw_-300x200.png" style="height:110px; margin:auto;">
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
											Hakkında
										</a>
									</h4>
								</div>
								<div id="collapse1" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											1985 yılında taahhüt faaliyetleriyle İstanbul Beşiktaş’taki bir dükkanda ticaret hayatına başlayan ODE, 1988 yılında Türkiye’nin en çok ihtiyaç duyduğu alanlardan birine yönelerek yalıtım sektöründe ilerleme kararı aldı.
										</p>
										<p>
											1990 yılında yurtdışından yalıtım malzemeleri ithal ederek Türkiye’de satışını yapan ODE, bu alanda 10 yıla yaklaşan deneyimini 1996 yılında üretimle birleştirdi. Uluslararası kalitedeki ürünleri pazara sunarak yalıtım sektörüne adım atan ODE, üretici kimliğiyle yaptığı tüm yatırımları hayata geçirirken Türkiye’de yapılacak üretimin de en iyisi olması gerektiği inancını her zaman korudu.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
											Isı Yalıtımı Nedir?
										</a>
									</h4>
								</div>
								<div id="collapse2" class="accordion-body collapse">
									<div class="panel-body">
										<p>
											Yapılarda ve tesisatlarda ısı kayıp ve kazançlarının sınırlandırılması için yapılan isleme “ısı yalıtımı” denir. Teknik olarak, ısı yalıtımı, farklı sıcaklıktaki iki ortam arasında ısı geçisini azaltmak için uygulanır
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
											Isı Yalıtımının Avantajları
										</a>
									</h4>
								</div>
								<div id="collapse3" class="accordion-body collapse">
									<div class="panel-body">
										<ul>
											<li>Isı Yalıtımı Enerji Tüketimini Azaltır</li>
											<li>Isı Yalıtımı Çevrenin Korunmasına Katkı Sağlar</li>
											<li>Isı Yalıtımı Isıl Konfor Sağlar</li>
											<li>Isı Yalıtımı Sağlıklı Yaşam Sunar</li>
											<li>Isı Yalıtımı İlk Yatırım ve İşletme Maliyetlerini Azaltır</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">JOTUN</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/1482763516_jotun3.jpg" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/fenamastik.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/Jotun_topcoat_tcm39-31614.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/jotun-1.png" style="height:110px; margin:auto;">
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
											Hakkında
										</a>
									</h4>
								</div>
								<div id="collapse6" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											Jotun Grup, dünya çapında kendi içerisinde 7 bölgeye ayrılarak Dekoratif Boyalar ve Performans Boyaları (Deniz, Endüstri, Toz Boyalar) alanında faaliyet göstermektedir. 19 ülkede 36 fabrika, 45 ülkede 74 şirket ile 90’dan fazla ülkede temsil edilmektedir. Jotun, çalışanlarını, müşterişelerini, tedarikçilerini, hissedarlarını ve çevreyi korur.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
											Yapıları Korur
										</a>
									</h4>
								</div>
								<div id="collapse7" class="accordion-body collapse">
									<div class="panel-body">
										<p>
											Jotun ürünleri likit (solvent veya su bazlı) ve toz halde, belli özellikler ile donatılmış şekilde ve son kullanıcıya katma değer sağlamak üzere tasarlanırlar.
										</p>
										<p>
											Boyalı yüzeye çok iyi bir görünüm (dekoratif özellikler) kazandırmanın yanı sıra, ürünlerin asıl fonksiyonu yüzeyi birçok dış etkenden korumaktır. Jotun ürünleri, hangi dış etkenlere karşı koruma sağladıklarına bakılarak gruplandırılabilirler.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse8">
											Faliyet Alanlarımız
										</a>
									</h4>
								</div>
								<div id="collapse8" class="accordion-body collapse">
									<div class="panel-body">
										<ul>
											<li>Jotun Toz Boyalar</li>
											<li>Beyaz eşya</li>
											<li>Mobilya</li>
											<li>Bina bileşenleri</li>
											<li>Boru hatları ve diğer genel sektörlerde faaliyet gösteren firmalara hizmet verir.</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse9">
											Ürün Performansı
										</a>
									</h4>
								</div>
								<div id="collapse9" class="accordion-body collapse">
									<div class="panel-body">
										<p>Ürün testleri kürlenmiş ve boyanmış panellerin farklı şartlara maruz bırakılmasıyla çeşitli şekillerde yapılır.</p>

										<p>Boyanın koruyucu özelliklerini hem doğal, hem de hızlandırılmış testlerle değerlendiririz.</p>

										<p>Hızlandırılmış korozyon testleri boyalı panelleri deniz tuzu gibi ileri derecede etkili ortamlarda gerçekleşir ve böylece paslanma direncinin limitleri ölçülür. Doğal ve hızlandırılmış iklimlendirme testleri dış mekanda boya filminin bütünlüğünü ve parlaklık dayanıklılığını ölçer.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">ABS Alçı</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/1482763795_abs-alci-logo.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/abs-alci-2.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/abs-alci-3.png" style="height:110px; margin:auto;">
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse10">
											Hakkında
										</a>
									</h4>
								</div>
								<div id="collapse10" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											ABS Alçı ve Blok Sanayi A.Ş. 1979 yılında kurulmuş ve ilk yatırımını Bilecik/Bozüyük`de gerçekleştirerek 1982 yılında 67.000 ton/yıl kapasite ile üretime başlamış ve 2008 itibari ile 1.885.000 ton/yıl kapasiteye ulaşmıştır. Günümüz verileri itibariyle ABS`nin üretim ve satışını gerçekleştirdiği ürünlerin % 65`i iç pazarda, % 35`i dış pazarlara sunulmaktadır.
										</p>
										<p>
											ABS öncesine kadar sadece tamirat amaçlı ve dekoratif malzeme üretiminde kullanılan Alçı; ABS ile yeni bir boyut kazanmış ve türk inşaat sektörü; Alçı Blok, Sıva Alçısı, Saten Perdah Alçısı, Dolgu ve Yapıştırıcı Alçıları gibi yeni ürünler ile tanışmıştır. Yıllar içinde bu ürünler, aranılan inşaat malzemeleri arasında layık olduğu yeri almıştır.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse11">
											Neden ABS Alçı?
										</a>
									</h4>
								</div>
								<div id="collapse11" class="accordion-body collapse">
									<div class="panel-body">
										<p>
											ABS ALÇI VE BLOK SANAYİ A.Ş.’nin sektördeki kaliteli ürün imajını dikkate alarak, kalite yönetim sisteminin sürekli iyileştirilmesini sağlamak,şirketin kararlı,ekonomik ve dinamik yapısını her geçen gün daha ileriye götürmek ve şirketimize olan güven duygusunu daha da arttırarak müşteri memnuniyetini en üst düzeye çıkarmak temel hedefimizdir.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse12">
											Misyonumuz
										</a>
									</h4>
								</div>
								<div id="collapse12" class="accordion-body collapse">
									<div class="panel-body">
										<p>Müşteri beklentileri doğrultusunda uygun ürünleri; çevre ve insan sağlığına saygılı bir şekilde üretmek, bunun için gerekli AR-GE faaliyetlerini düzenli olarak sürdürmek,teknolojik gelişmeleri yapılacak yeni yatırımlarla desteklemek,iç müşterilerin kişisel gelişimleri için politikalar üretmek,dış müşterilerin pazar koşulları gereği beklentilerini azami olarak karşılamak.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">DYO</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/dyo_logo.gif" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/DYO0040.jpg" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/dyo-dinamik-1200x750.png" style="height:110px; margin:auto;">
							</div>

						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse13">
											Hakkında
										</a>
									</h4>
								</div>
								<div id="collapse13" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											Dyo Boya Fabrikaları Sanayi ve Ticaret A.Ş. 1927 yılında Durmuş Yaşar tarafından İzmir'de küçük bir mağaza olarak kurulmuştur. 1941 yılında ilk boya imalatını gerçekleştiren şirket, 1954'de ilk boya fabrikasını açarak boya sektöründe önemli bir ilerleme katetti. 1957'de ise Arev Boya Pazarlama Şirketi kuruldu.

											1958'de Dyo, reçine üretim ve imalatına başlayarak, dış pazara ilk adımını attı. 1964'te mikronize mineraller fabrikası ve Dyo Harris Boya Fırçaları fabrikasını, 1965'te Dyosad fabrikasını ve 1968'de Dyo Matbaa Mürekkepleri Fabrikası'nı açarak sektördeki etkinliğini arttırmaya çalıştı. 1969'da, polyester üretimine başlandı ve Dyo Araştırma Geliştirme Laboratuvarları, Dyo Transocean Deniz Boyaları Fabrikası açılarak ürün sahasını genişletti.

											1979'da, Bayiler ve Dyo, Yasaş adı altında birleştirildi ve Yasaş Yaşar Boya ve Kimya Sanayi ve Ticaret A.Ş. olarak devam etti. Bayraklı boya fabrikası ve polyester fabrikası satın alınıp, Yasaş Gebze fabrikası açılarak içte ve 1982'de Yadex/Almanya şirketi kurularak dışta büyümesini hızlandırdı.

											Bundan başka Dyo, akrilik sistem üretimi, mixing sistemi, Dyo bilgi işlem sistemi gibi yeniliklerle gelişmesine devam etti.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse14">
											Faliyet Alanlarımız
										</a>
									</h4>
								</div>
								<div id="collapse14" class="accordion-body collapse">
									<div class="panel-body">
										<ul>
											<li>Nanoteknolojik Boyalar</li>
											<li>Emülsiyon ve sentetik son kat boyalar</li>
											<li>Dış cephe son kat boya ve kaplamaları</li>
											<li>Ahşap koruyucular</li>
											<li>Vernikler, parke cilaları</li>
											<li>Renk Pınarı Sistemi</li>
											<li>Sorun çözücü özel ürünler (Dr. DYO ürünleri)</li>
											<li>Astarlar, macunlar</li>
											<li>Fırça-Rulo-Boya uygulama makineleri</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">STONEWRAP</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/stonewrap.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/stonewrap1.jpg" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/stonewrap2.jpg" style="height:110px; margin:auto;">
							</div>

						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse15">
											Hakkında
										</a>
									</h4>
								</div>
								<div id="collapse15" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											1998 yılında başladığımız kültür taşı üretimimiz, bugün StoneWrap kültür taşı, UrbanBrick kültür tuğlası ve UrbanFloor yer taşı tescilli markalarıyla ulusal ve uluslararası pazarda hizmet vermektedir. Bugün tasarımcılara 45 farklı dokuda, 150 farklı renkte ürün sunarak tasarımcıların her türlü projesinde çözüm sunmaktadır.
										</p>
										<p>
											Ürünlerimiz ISO 9001 kalite standartlarına göre kalite kontrolünden geçerek sevk edilmektedir.
										</p>
										<p>
											Firmamız, uzman satış ekibiyle yurtiçinde ve yurtdışında oluşturduğu bayi-satış noktası ağıyla geniş bir coğrafyada yaygın ve iyi bir hizmet vermeyi amaç edinmiştir.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse16">
											Ürünler
										</a>
									</h4>
								</div>
								<div id="collapse16" class="accordion-body collapse">
									<div class="panel-body">
										<ul>
											<li>Stonewrap - Kültür Taşı</li>
											<li>Urban Brick - Kültür Tuğlası</li>
											<li>Urban Floor - Yer Taşı</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">ŞERİFOĞLU PARKE</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/şerifoğlu.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/parke2.png" style="height:110px; margin:auto;">
							</div>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/parke3.png" style="height:110px; margin:auto;">
							</div>

						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse17">
											Hakkında
										</a>
									</h4>
								</div>
								<div id="collapse17" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											1967 yılından bu yana, gerçek ahşap deyince, akla ilk gelen markadır Şerifoğlu.

											Düzce merkezli olarak kurulan, masif parke üretimi ile başlayan yolculuğuna bu konuda kazandığı

											birikim ve tecrübesini katarak Avrupa'nın en çok tercih edilen ürünü olan lamine parke ile devam

											ederken, ahşaptan hiçbir zaman vazgeçmemiştir.
										</p>
										<p>
											Sektördeki lider konumunu koruyabilmek ve kalıcı olabilmek adına, önceliği olan kalite ve müşteri

											memnuniyetini hedef edinerek yoluna devam eden Şerifoğlu, teknolojik gelişmeler ve yenilikler

											ışığında, dünya pazarını yakından takip ederek ilerlemektedir.										</p>
										<p>
											Dünya standartlarındaki üretimini 105.000 m² açık, 45.000 m² kapalı alan üzerinde kurulu olan

											fabrikalarında gerçekleştirmektedir. Yıllık 2.000.000 m² üretimi ile Türkiye'nin ilk ve en büyük

											lamine parke üreticisi olan Şerifoğlu, kuruluşundan bu yana hedeflerinden ve kalitesinden ödün

											vermeyerek yoluna devam etmektedir. Yurt içinde gün geçtikçe yayılan bayi ağı ve üretiminin

											%30'unu oluşturan ihracatı ile Türkiye'de olduğu gibi, uluslararası pazarda da aranan bir marka

											haline gelmiştir.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse18">
											Vizyon
										</a>
									</h4>
								</div>
								<div id="collapse18" class="accordion-body collapse">
									<div class="panel-body">
										<p>
											Güvenilirlik ve kalite ilkelerinden vazgeçmeden, müşteri memnuniyeti ve ihtiyaçlarını esas

											alarak, kendi doğrularımız çerçevesinde sektörümüzde aranan bir marka olarak kalmak ve

											yenilikçi ruhumuzla istikrar içinde büyüyen bir şirket olarak uluslararası pazardaki yerimizi

											güçlendirmek ve uzun vadede kalıcı olmasını sağlamak.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse19">
											Değerler
										</a>
									</h4>
								</div>
								<div id="collapse19" class="accordion-body collapse">
									<div class="panel-body">
										<ul>
											<li>Müşteri memnuniyeti</li>

											<li>Kalite odaklılık</li>

											<li>Güvenilirlik</li>

											<li>Teknolojiyi takip ve Yenilikçilik</li>

											<li>Üstün iş ahlakı</li>

											<li>Dürüst çalışma ilkeleri</li>

											<li>Takım çalışması</li>

											<li>Yüksek hizmet kalitesi</li>

											<li>Etik değerlere uyum</li>

											<li>Mükemmeli hedefleyen anlayışımız</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<h3 class="heading-primary" style="text-align:center;">YAPKİM</h3>
						<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1,"autoplay": true, "autoplayTimeout": 30000, "dots": false}'>
							<div style="text-align:center;">
								<img alt="" class="img-responsive img-rounded" src="/img/seller/yapkim.png" style="height:110px; margin:auto;">
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse20">
											Hakkında
										</a>
									</h4>
								</div>
								<div id="collapse20" class="accordion-body collapse in">
									<div class="panel-body">
										<p>
											YAPKİM, yapı kimyasalları sektöründe yer alan tüm segmentlere hitap eden ürünleri üreterek, geliştirerek ve yenileyerek hep farklı olmayı hedefleyen %100 Türk sermayesiyle yerli yatırım yapmış olup; 12.000 m2’lik fabrika alanı, 7.000 m2 kapalı üretim tesisi ve toz, reçine, likit üretim hatları ile sektörde yerini alarak Türkiye ekonomisine katkı sağlamaktadır. YAPKİM, sektörde en iyi olmayı sürdürmenin sürekli ürün gamını geliştirmekten geçtiğini bilerek AR-GE çalışmalarına önemli bütçeler ayırmaktadır. Kurumsal hedefimiz, güçlü AR-GE ve ileri teknolojiye sahip 3 laboratuvar desteğiyle hazırlanan ürünlerimizin, kalite ve ürün standartlarından ödün vermeden, güvence ve doğru sistem detayları sunan yapımızı ileriye yönelik olarak sürdürmektir. Yüksek kalitesini ulaşılabilir bir ekonomide müşterilerine sunan, ileri teknoloji ürünlere sahip ve süreklilik gösteren üst düzey bir firma olmak YAPKİM’in en önemli ilkelerinden biridir. Yapı kimyasalları sektöründe güvenli, doğru sistem detaylı ve tüm segmentlerde gerekli olan ihtiyaçları karşılayacak ürünleri üretmeye odaklanmıştır. YAPKİM, bayi ve müşterilerini iş ortağı olarak görür. Kalitesi ve ulaşılabilirliği ile servis veren bayisinin güvenilirliğini birleştirerek müşterisinde alışkanlık yaratır. Sektörün ihtiyaçlarını ve gelişmelerini takip ederek inovatif sistem çözümleri yaratır ve gücünü gelecek için kurar. Temelden çatıya kadar tüm yapı kimyasalları sektörüne hizmet veren YAPKİM, güçlü teknolojik bilgi birikimi, güçlü AR-GE’si ve sunduğu komple sistem çözümleri ile geniş bir hizmet ağına sahiptir.
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse21">
											Ürünler
										</a>
									</h4>
								</div>
								<div id="collapse21" class="accordion-body collapse">
									<div class="panel-body">
										<ul>
											<li>YAPBOND
												<ul>
													<li>Seramik Yapıştırıcıları</li>
													<li>Derz dolguları ve Astarlar</li>
												</ul>
											</li>
											<li>YAPBUILT
												<ul>
													<li>Onarım Güçlendirme</li>
												</ul>
											</li>
											<li>YAPFLOOR
												<ul>
													<li>Zemin Kaplama Sistemleri</li>
												</ul>
											</li>
											<li>YAPGUARD
												<ul>
													<li>Tarihi Yapı Restorasyonu</li>
													<li>Yüzey Koruma Malzemeleri</li>
												</ul>
											</li>
											<li>YAPRHEO
												<ul>
													<li>Beton Katkıları</li>
													<li>Harç Katkıları</li>
													<li>Beton Yan Ürünleri</li>
													<li>Yer Altı Kimyasalları</li>
												</ul>
											</li>
											<li>YAPSEAL
												<ul>
													<li>Su yaltımı</li>
													<li>Isı yalıtımı</li>
													<li>Mastikler</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>


@endsection