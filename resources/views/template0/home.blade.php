@extends('layouts.template0.master')

@section('content')

<div role="main" class="main">
                <div class="slider-container rev_slider_wrapper">
                    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 500}'>
                        <ul>
                            <?php foreach ($sliders as $key => $slider): ?>
                                
                            
                            <li data-transition="fade">
                                <a href="{{ $slider->link }}">
                                <img style="width:100%; height:100%;" src="{{ $slider->image }}"
                                    alt=""
                                    data-bgposition="center center" 
                                    data-bgfit="cover" 
                                    data-bgrepeat="no-repeat" 
                                    class="rev-slidebg">

                                <div class="tp-caption featured-label"
                                    data-x="center"
                                    data-y="210"
                                    data-start="500"
                                    style="z-index: 5"
                                    data-transform_in="y:[100%];s:500;"
                                    data-transform_out="opacity:1;s:500;">{{ $slider->title }}</div>

                                <div class="tp-caption bottom-label"
                                    data-x="center"
                                    data-y="270"
                                    data-start="1000"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:600;e:Power4.easeInOut;"
                                    data-transform_out="opacity:0;s:500;"
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                    data-splitin="chars" 
                                    data-splitout="none" 
                                    data-responsive_offset="on"
                                    style="font-size: 23px; line-height: 30px;"
                                    data-elementdelay="0.05">{{ $slider->subtitle }}</div>
                                </a>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="home-intro home-intro-secondary" id="home-intro">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-8">
                                <p>
                                    <em>Haliç Yapı Dekorasyon</em>
                                    <span>Yaptıklarımız Yapacaklarımızın Teminatıdır.</span>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="get-started">
                                    <a href="/iletisim" class="btn btn-lg btn-primary">Bize Ulaşın</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="container">
                    <div class="row mb-xl">
                        <div class="col-md-12 center">

                            <h2 class="mb-xl"><strong>Hakkımızda</strong></h2>
                            <p>2002 Tarihinden bugüne yapmış olduğumuz geniş kapsamlı ve güçlü refaranslarımızla, inşaat sektöründe malzeme satışları ve uygulama sistemlerimizi teknolojinin getirdiği yeniliklerle güçlendirip sürekli sizle sayesinde emin adımlarla sizlere hizmet vermekteyiz.</p>

                            <!--<a class="btn btn-borders btn-default mr-xs mb-sm" href="/">Devamı</a>-->
                        </div>
                    </div>
                </div></div>
                <section class="section section-tertiary section-no-border mb-none">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 center">
                                <h2 class="heading-dark"><strong>Neler Yapıyoruz?</strong></h2>
                            </div>
                        </div>

                        <div class="row">
                            <?php foreach ($services as $key => $service): ?>
                                <div class="col-md-4" style="min-height: 240px !important;">
                                    <div class="feature-box feature-box-style-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                                        <div class="feature-box-icon" align="center">
                                            @if($service->title == "Taş Kaplama")
                                                <img src="/img/neler-yapiyoruz/taskaplama.png" style="height:38px;">
                                            @elseif($service->title == "Seramik Uygulamalar")
                                                <img src="/img/neler-yapiyoruz/seramikuygulamalar.png" style="height:38px;">
                                            @elseif($service->title == "Alçıpan Uygulamaları")
                                                <img src="/img/neler-yapiyoruz/alcipanuygulamalari.png" style="height:38px;">
                                            @elseif($service->title == "Şap Uygulamaları")
                                                <img src="/img/neler-yapiyoruz/sapuygulamalari.png" style="height:38px;">
                                            @elseif($service->title == "Parke Uygulamaları")
                                                <img src="/img/neler-yapiyoruz/parkeuygulamalari.png" style="height:38px;">
                                            @elseif($service->title == "Çatı İşlemleri")
                                                <img src="/img/neler-yapiyoruz/cati-islemleri.png" style="height:38px;">
                                            @elseif($service->title == "Boya İşleri")
                                                <img src="/img/neler-yapiyoruz/boyama.png" style="height:38px;">
                                            @elseif($service->title == "İzolasyon")
                                                <img src="/img/neler-yapiyoruz/izolasyon.png" style="height:38px;">
                                            @elseif($service->title == "Mobilya Uygulamaları")
                                                <img src="/img/neler-yapiyoruz/dolap.png" style="height:38px;">
                                            @else
                                                <i class="fa fa-star-o"></i>
                                            @endif
                                        </div>
                                        <div class="feature-box-info">
                                            <h4 class="mb-sm">{{ $service->title }}</h4>
                                            <p class="mb-lg">{{ $service->short_content }}</p>
                                        </div>
                                    </div>
                                </div>  
                            <?php endforeach ?>
                        </div>
                    </div>
                </section>
                <section class="section section-quaternary section-no-border mt-none">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 center">
                                <h2 class="heading-dark"><strong>Müşteri Yorumları</strong></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1, "margin": 0, "loop": false}'>
                                <?php foreach ($comment as $key => $client): ?>
                                    <div class="col-md-12">
                                        <div class="testimonial testimonial-style-4 testimonial-no-borders appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="0">
                                            <div class="testimonial-author" style="max-width: 220px;margin: 0 auto;text-align: center;">
                                                <div class="testimonial-author-thumbnail">
                                                    <?php if ($client->image): ?>
                                                        <img style="height:60px !important;" src="{{$client->image}}" class="img-responsive img-circle" alt="">
                                                    <?php else: ?>
                                                        <img style="height:60px !important;" src="/img/icon-person.png" class="img-responsive img-circle" alt="">
                                                    <?php endif ?>
                                                </div>
                                                <p><strong>{{ $client->name }}</strong><span>{{ $client->position }}</span></p>
                                            </div>
                                            <blockquote style="text-align:center;max-width:650px; margin:0 auto;">
                                                <p><?=$client->comment?></p>
                                            </blockquote>
                                            <div class="testimonial-arrow-down"></div>
                                           
                                        </div>
                                    </div>
                                <?php endforeach ?>
                                
                            </div>
                        </div>
                    </div>
                </section>
                <style type="text/css">
                    .owl-dots {
                        display: block !important;
                    }
                </style>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 center">
                            <h2 class="mt-xl"><strong>Bayiliklerimiz</strong></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 center">
                            <div class="owl-carousel owl-theme mt-xl" data-plugin-options='{"items": 4, "autoplay": true, "autoplayTimeout": 3000}'>
                                <?php /* foreach ($sellers as $key => $seller): ?>
                                <div>
                                    <a href="/bayiliklerimiz"><img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="<?=$seller->image?>" alt="">
                                    </a>
                                    <!--<?php if ($seller->image): ?>
                                        <a href="{{$seller->link}}"><img style="padding:10px;" class="img-responsive" src="<?=$seller->image?>" alt=""></a>
                                    <?php else: ?>
                                        <h1> {{$seller->name}} </h1>
                                    <?php endif ?>-->
                                </div>
                                <?php endforeach; */?>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/1482763830_BASF.png" alt="BASF">
                                    </a>
                                </div>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/1482763724_ode-logo.jpg" alt="ODE">
                                    </a>
                                </div>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/1482763516_jotun3.jpg" alt="JOTUN">
                                    </a>
                                </div>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/1482763795_abs-alci-logo.png" alt="ABS ALÇI">
                                    </a>
                                </div>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/dyo_logo.gif" alt="DYO">
                                    </a>
                                </div>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/stonewrap.png" alt="STONEWRAP">
                                    </a>
                                </div>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/şerifoğlu.png" alt="ŞERİFOĞLU PARKE">
                                    </a>
                                </div>
                                <div>
                                    <a href="/bayiliklerimiz">
                                        <img style="padding:10px; height:80px;margin:auto;" class="img-responsive" src="/img/seller/yapkim.png" alt="YAPKİM">
                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
@endsection
