<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

      <?php
        $setting = App\Setting::where('id',1)->first();
     ?>
    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/template1/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/swiper.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/dark.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/animate.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="/css/template1/responsive.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/construction.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/css/template1/main.css" type="text/css" />

    <!-- Document Title
    ============================================= -->
    <title>{{$allSetting->name}}</title>

</head>

<body class="stretched no-transition">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">
  <!-- Header
  ============================================= -->
  <header id="header" class="sticky-style-2">

    <div class="container clearfix">

      <!-- Logo
      ============================================= -->


    </div>
    <div id="header-wrap">

      <!-- Primary Navigation
      ============================================= -->
      <nav id="primary-menu" class="with-arrows style-2 center">
        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
        <div class="container clearfix" style="padding-bottom:13px;">
          <div id="logo">
              <a href="/" class="standard-logo" data-dark-logo="{{$allSetting->logo}}">
                <img src="{{$allSetting->logo}}" alt="{{$allSetting->name}}" style="padding:8px 0px 8px 0px;">
              </a>
              <a href="/" class="retina-logo" data-dark-logo="{{$allSetting->logo}}">
                <img src="{{$allSetting->logo}}" alt="{{$allSetting->name}}">
              </a>
          </div><!-- #logo end -->

          <ul id="padding">
            <li><a href="/"><div>ANASAYFA</div></a>
            </li>
            @foreach ($mainMenu as $key => $menu)
            <li>
              <a href="/{{$menu->slug}}"><div>{{$menu->title}}</div></a>
              @if($menu->parent == 0)
                @foreach($subMenu as $sub)
                @if($menu->id == $sub->parent)
                <ul>
                  <li>
                    <a href="/{{$sub->slug}}"><div>{{$sub->title}}</div></a>
                  </li>
                </ul>
                @endif
                @endforeach
              @endif
            </li>
              @endforeach
            <li><a href="/iletisim"><div>İLETİŞİM</div></a>
            </li>
          </ul>

          <!-- Top Search
          ============================================= -->
          <div id="top-search">
            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
            <form action="search.html" method="get">
              <input type="text" name="q" class="form-control" value="" placeholder="Arama Yapınız...">
            </form>
          </div><!-- #top-search end -->

        </div>

      </nav><!-- #primary-menu end -->

    </div>

  </header><!-- #header end -->

    @yield('content')

    <!-- Footer
		============================================= -->
    <footer id="footer" class="dark">

            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix">

                    <div class="col_two_third">

                        <div class="col_one_third">

                            <div class="widget clearfix">

                                <img src="{{$allSetting->logo}}" alt="" class="footer-logo">



                                <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                                    <address>
                                        <strong>Adres:</strong><br>
                                        {{$setting->address}}
                                        <br>

                                    </address>
                                    <strong>Tel:</strong> {{$setting->phone}}<br>
                                    <strong>Fax:</strong> {{$setting->fax}}<br>
                                    <strong>Email:</strong> {{$setting->email}}
                                </div>

                            </div>

                        </div>

                        <div class="col_one_third">

                            <div class="widget widget_links clearfix"  style="padding-left:50px;">

                                <h4>Sayfalar</h4>
                                <ul>
                                  <li><a href="/" style="padding:0px;"><div>Anasayfa</div></a></li>
                                  <?php foreach ($mainMenu as $key => $menu): ?>
                                  <li>
                                    <a href="/<?=$menu->slug?>" style="padding:0px;"><div><?=$menu->title?></div></a>
                                  </li>
                                  <?php endforeach ?>
                                  <li><a href="/iletisim" style="padding:0px;"><div>İletişim</div></a></li>
                                </ul>

                            </div>

                        </div>

                        <div class="col_one_third col_last">

                            <div class="widget clearfix">
                                <h4>HİZMETLERİMİZ</h4>

                                <div id="post-list-footer">
                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Proje Hizmetleri</a></h4>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Taahhüt Hizmetleri</a></h4>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Danışmanlık Hizmetleri</a></h4>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col_one_third col_last">
                     <div class="widget clearfix">

                    <h4>sosyal medya hesaplarımız</h4>

                        <div class="widget clearfix" style="margin-bottom: -20px;">
                         <div class="row">

                                <div class="col-md-8 clearfix bottommargin-sm">
                                    <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>

                                    <a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>

                                    <a href="https://www.instagram.com/ussmimarlik/" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-instagram"></i>
                                        <i class="icon-instagram"></i>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="widget subscribe-widget clearfix">

                            <h5>Sizlerde kampanyalarımızdan haberdar olmak isterseniz aşağıdaki bölüme mail adresinizi yazıp göndermeniz yeterlidir.</h5>
                            <div class="widget-subscribe-form-result"></div>
                            <form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                                <div class="input-group divcenter">
                                    <span class="input-group-addon"><i class="icon-email2"></i></span>
                                    <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Mail adresinizi yazınız">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit">Gönder</button>
                                    </span>
                                </div>
                            </form>
                        </div>

                        <div class="widget clearfix" style="margin-bottom: -20px;">


                        </div>

                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>
            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">
                <div class="container clearfix">
                <div class="col-sm-6" style="padding-left:0px">
                        Uss Mimarlık &copy; Copyright 2018. Tüm Hakları Saklıdır.<br>
                </div>
                <div class="col-sm-6 cozum text-right" style="padding:0px;">
                       Design and Software <a href="http://www.cozumlazim.com" target="_blank"> Çözüm Lazım</a>
                </div>
                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="/js/template1/jquery.js"></script>
<script type="text/javascript" src="/js/template1/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/js/template1/functions.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBeSHmI1J4Cg0aGVTjD2U9-avXFaPx50oo"></script>
<script type="text/javascript" src="/js/template1/jquery.gmap.js"></script>
<script>
// setTimeout(function(){
//   $('ol').attr('style','display:none')
// },1000);
// });
    jQuery(document).ready( function($){
        $('#shop').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("$");

                    return parseFloat( priceNum[1] );
                },
                price_hl: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("$");

                    return parseFloat( priceNum[1] );
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
            var element = $(this),
                    elementFilter = element.children('a').attr('data-filter'),
                    elementFilterContainer = element.parents('.custom-filter').attr('data-container');

            elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

            element.append('<span>'+ elementFilterCount +'</span>');

        });

        $('.shop-sorting li').click( function() {
            $('.shop-sorting').find('li').removeClass( 'active-filter' );
            $(this).addClass( 'active-filter' );
            var sortByValue = $(this).find('a').attr('data-sort-by');
            $('#shop').isotope({ sortBy: sortByValue });
            return false;
        });
    });
</script>
<script type="text/javascript">
<?php
  $setting = App\Setting::where('id',1)->first();
?>
    $('#google-map').gMap({
        latitude: {{$setting->latitude}},
        longitude: {{$setting->longitude}},
        maptype: 'ROADMAP',
        zoom: 15,
        markers: [
            {
                latitude: {{$setting->latitude}},
                longitude: {{$setting->longitude}},
                html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">USS MİMARLIK İNŞAAT SANAYİ VE TİCARET ANONİM ŞİRKETİ </h4></div>',
                icon: {
                    image: "images/icons/map-icon-red.png",
                    iconsize: [32, 39],
                    iconanchor: [32,39]
                }
            }
        ],
        doubleclickzoom: false,
        controls: {
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false
        }
    });
</script>
</body>
</html>
