@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style="background-image: url('/img/template1/cover.jpg');"></div>

    <div class="container clearfix">
        <h1><?=$insaat[0]['title']?></h1>
        <span>FAALİYET ALANLARIMIZ - İNŞAAT VE TAAHHÜT</span>
        <ol class="breadcrumb">
            <li><a href="#">Anasayfa</a></li>
            <li class="active">İnşaat ve Taahhüt</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap" style="padding:50px;">

    <div class="container clearfix">

      <div class="row clearfix">
        <div class="col-md-12">
          <div class="heading-block nobottomborder bottommargin-sm">
            <?=$insaat[0]['content']?>
          </div>
        </div>
      </div>
    </div>

  </div>

</section><!-- #content end -->

@endsection
