@extends('layouts.template1.master')

@section('content')
<section id="slider" class="slider-parallax swiper_wrapper clearfix" style="height: 550px;" data-loop="true">

  <div class="swiper-container swiper-parent">
    <div class="swiper-wrapper">
      @foreach($sliders as $slider)
      <div class="swiper-slide" style="background-image: url('{{$slider->image}}'); background-position: center top;">
        <div class="container clearfix">
        </div>
      </div>
      @endforeach
    </div>
    <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
    <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
  </div>

</section>

<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="promo promo-light promo-full uppercase bottommargin-lg header-stick">
      <div class="container clearfix">
        <h3 style="letter-spacing: 2px;">HAYALLERİNİZDEKİ PLANLARI HAYATA GEÇİRİN</h3>
        <a href="/iletisim" class="button button-large button-border button-rounded">BİZE ULAŞIN</a>
      </div>
    </div>

    <div class="container clearfix">

      <div class="col_one_third nobottommargin">
        <div class="feature-box media-box">
          <div class="fbox-media">
            <img style="border-radius: 2px;" src="images/services/taahhut-hizmetleri.jpg" alt="Why choose Us?">
          </div>
          <div class="fbox-desc">
            <h3>PROJE HİZMETLERİ</h3>
              <p>
                <li>Ruhsat Projeleri</li>
                <li>Mimari Uygulama Projeleri</li>
                <li>Statik Projeler</li>
                <li>Elektrik ve Mekanik Projeler</li>
              </p>
          </div>
        </div>
      </div>

      <div class="col_one_third nobottommargin">
        <div class="feature-box media-box">
          <div class="fbox-media">
            <img style="border-radius: 2px;" src="images/services/1.jpg" alt="Effective Planning">
          </div>
          <div class="fbox-desc">
            <h3>TAAHHÜT HİZMETLERİ</h3>
              <p>
                <li>Süpermarket ve Hipermarketler</li>
                <li>Banka Şubeleri</li>
                <li>Ofisler</li>
                <li>Otel ve Tatil Köyleri</li>
                <li>Hastaneler</li>
                <li>Konut İşleri</li>
              </p>
          </div>
        </div>
      </div>

      <div class="col_one_third nobottommargin col_last">
        <div class="feature-box media-box">
          <div class="fbox-media">
            <img style="border-radius: 2px;" src="images/services/danismanlik_hizmetleri.jpeg" alt="Why choose Us?">
          </div>
          <div class="fbox-desc">
            <h3>DANIŞMANLIK HİZMETLERİ</h3>
              <p>
                <li>Proje Danışmanlık Hizmeti</li>
                <li>Tesis İnşaat Kontrollük Hizmeti</li>
                <li>Bina/Arsa uyumluluğu Danışmanlık Hizmeti</li>
              </p>
          </div>
        </div>
      </div>

    </div>
    <div class="section nobottommargin">
      <h2 class="center nobottommargin ls1">HİZMETLERİMİZ</h2>
    </div>
    <!-- <div class="section parallax dark" style="background-image: url('images/slider/1.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.4">

      <div class="fslider testimonial testimonial-full" data-arrows="false" style="z-index: 2;">
        <div class="flexslider">
          <div class="slider-wrap">
            <?php $myorum = App\OurClients::where('status',1)->get(); ?>
            @foreach($myorum as $yorum)
            <div class="slide">
              <div class="testi-image">
                @if($yorum->image)
                <a href="#"><img src="{{$yorum->image}}" alt="Customer Testimonails"></a>
                @else
                <a href="#"><img src="images/testimonials/3.jpg" alt="Customer Testimonails"></a>
                @endif
              </div>
              <div class="testi-content">
                <p>{{$yorum->comment}}</p>
                <div class="testi-meta">
                  {{$yorum->name}}
                  <span style="color:#383535;"><strong>{{$yorum->position}}</strong></span>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>

      <div class="video-wrap" style="z-index: 1;">
        <div class="video-overlay" style="background: rgba(241,128,82,0.9);"></div>
      </div>

    </div> -->

    <div class="container clearfix" style="padding-top:50px;">

      <div class="clear-bottommargin">
        <div class="row common-height clearfix">
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <!-- <a href="#"><img src="images/icons/building.png" alt="Concrete Developments"></a> -->
                  <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>Akıllı Yapılar</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>YENİLİKÇİ TASARIMLAR</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>MODERN DEKORASYON</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>UYGUN TASARIMLAR</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>GÜVENLİ YAPILAR</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>KALİTELİ İŞÇİLİK</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>KABA BETONARME</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>DUVAR KONSTRÜKSİYON</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>KABA & İNCE SIVA</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ALÇI SIVA</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>DIŞ CEPHE BRÜT YÜZEY TAMİRATI</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ŞAP TESVİYE BETONU</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>İZOLASYON</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>MANTOLAMA</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ALÇIPAN</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>KARTONPİYER</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>SERAMİK & FAYANS DÖŞEME</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>KARO & MERMER DÖŞEME</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>DIŞ İÇ CEPHE BOYAMA</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>MEKANİK ALT YAPI</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>HAVUZ</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>DIŞ CEPHE SERAMİK KAPLAMA</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>HAFRİYAT</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ÖZEL İMALAT MOBİLYA</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>SPOR ALANLARI</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>SAUNA,HAMAM</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>SİNEMA ODALARI</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>BİNA AKILLI SİSTEMLERİ</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ELEKTRİK VE TELEKOM ALTYAPI İMALATLARI</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>PEYZAJ</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>MİMARİ PROJE</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>STATİK PROJE</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ELEKTRİK,MEKANİK TESİSAT PROJELERİ</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ÇEVRE VE PEYZAJ PROJELERİ</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>3-D MAX PROJELER</h3>
              <p></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 bottommargin">
            <div class="feature-box fbox-plain">
              <div class="fbox-icon">
                <i class="fa fa-check" style="color:green;"></i>
              </div>
              <h3>ARSA & GAYRİMENKUL ALIM-SATIM</h3>
              <p></p>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="section nobottommargin">
      <h2 class="center nobottommargin ls1">REFERANSLARIMIZ</h2>
    </div>

    <div id="portfolio" class="portfolio grid-container portfolio-full clearfix">

      @foreach($referances as $referance)
      <article class="portfolio-item">
        <div class="product-image">
          <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="false" data-slideshow="false">
              <div class="flexslider">
                  <div class="slider-wrap" data-lightbox="gallery">
                      <div class="slide" data-thumb="">
                        <a href="{{$referance->image}}" title="" data-lightbox="gallery-item">
                          <img src="{{$referance->image}}" alt="">
                        </a>
                      </div>
                      <?php
                        $gallerys = App\Helpers\helper::alt_resim($referance->id);
                       ?>
                       @foreach($gallerys as $gallery)
                        <div class="slide" data-thumb="">
                          <a href="{{$gallery->image}}" title="" data-lightbox="gallery-item">
                            <img src="{{$gallery->image}}" alt="">
                          </a>
                        </div>
                       @endforeach
                  </div>
              </div>
          </div>
        </div><!-- Product Single - Gallery End -->
        <div class="portfolio-desc">
          <h3>{{$referance->name}}</h3>
          <span>{{$referance->refcontent.' '.$referance->link}}</span>
        </div>
      </article>
      @endforeach

    </div>
  </div>

</section><!-- #content end -->

@endsection
