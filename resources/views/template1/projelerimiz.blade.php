@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack"></div>

    <div class="container clearfix">
        <h1>PROJELERİMİZ</h1>
        <span>USS MİMARLIK Projeleri hakkında detaylı bilgiye buradan ulaşabilirsiniz.</span>
        <ol class="breadcrumb">
            <li><a href="#">Anasayfa</a></li>
            <li class="active">Projelerimiz</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="container clearfix">

      <div class="row clearfix">
        <div class="col-md-12">
          <div id="portfolio" class="portfolio grid-container portfolio-full clearfix">

            @foreach($referances as $referance)
            <article class="portfolio-item">
              <div class="product-image">
                <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="false" data-slideshow="false">
                    <div class="flexslider">
                        <div class="slider-wrap" data-lightbox="gallery">
                            <div class="slide" data-thumb="">
                              <a href="{{$referance->image}}" title="" data-lightbox="gallery-item">
                                <img src="{{$referance->image}}" alt="">
                              </a>
                            </div>
                            <?php
                              $gallerys = App\Helpers\helper::alt_resim($referance->id);
                             ?>
                             @foreach($gallerys as $gallery)
                              <div class="slide" data-thumb="">
                                <a href="{{$gallery->image}}" title="" data-lightbox="gallery-item">
                                  <img src="{{$gallery->image}}" alt="">
                                </a>
                              </div>
                             @endforeach
                        </div>
                    </div>
                </div>
              </div><!-- Product Single - Gallery End -->
              <div class="portfolio-desc">
                <h3>{{$referance->name}}</h3>
                <span>{{$referance->refcontent.' '.$referance->link}}</span>
              </div>
            </article>
            @endforeach
          </div>
        </div>
      </div>
    </div>

  </div>

</section><!-- #content end -->

@endsection
