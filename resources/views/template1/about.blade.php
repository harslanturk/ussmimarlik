@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack"></div>

    <div class="container clearfix">
        <h1><?=$aboutus[0]['title']?></h1>
        <span>USS MİMARLIK hakkında detaylı bilgiye buradan ulaşabilirsiniz.</span>
        <ol class="breadcrumb">
            <li><a href="#">Anasayfa</a></li>
            <li class="active"><?=$aboutus[0]['title']?></li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="container clearfix">

      <div class="row clearfix" style="margin-top: -80px;">
        <div class="col-md-12">
          <div class="heading-block nobottomborder bottommargin-sm">
            <h3>BİZ KİMİZ ?</h3>
            <?php
                $parcala = explode('<p>',$aboutus[0]['content']);
                //print_r($parcala);
             ?>
             <div class="col-sm-12" style="padding-left:0px;">
               <?php echo $parcala[1]; ?>
             </div>
             <div class="col-sm-8" style="padding-left:0px;">
               <?php echo $parcala[2]; ?>
              <?php echo $parcala[3]; ?>
             </div>
             <div class="col-sm-4">
               <img src="\img\setting\1514470168_uss.png" alt="\img\setting\1514470168_uss.png">
             </div>
             <div class="col-sm-12" style="padding-left:0px;">
              <?php echo $parcala[4]; ?>
             </div>
             <div class="col-sm-12" style="padding-left:0px;">
               <?php echo $parcala[5]; ?>
             </div>
             <div class="col-sm-12" style="padding-left:0px;">
               <?php echo $parcala[6]; ?>
             </div>
             <div class="col-sm-12" style="padding-left:0px;">
               <?php echo $parcala[7]; ?>
             </div>
             <div class="col-sm-12" style="padding-left:0px;">
               <?php echo $parcala[8]; ?>
             </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</section><!-- #content end -->

@endsection
