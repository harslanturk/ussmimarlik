@extends('layouts.template1.master')

@section('content')

	<!-- Google Map
		============================================= -->
	<section id="google-map" class="gmap slider-parallax"></section>

	<!-- Content
    ============================================= -->
	<section id="content">

		<div class="content-wrap">

			<div class="container clearfix">

				<!-- Postcontent
                ============================================= -->
				<div class="postcontent nobottommargin" style="width:860px;">

					<h3>BİZE ULAŞIN</h3>

					<div class="contact-widget">

						<div class="contact-form-result"></div>

						<form class="nobottommargin" id="template-contactform" name="template-contactform" action="/" method="post">

							<div class="form-process"></div>

							<div class="col_one_third">
								<label for="template-contactform-name">Ad Soyad <small>*</small></label>
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
							</div>

							<div class="col_one_third">
								<label for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
							</div>

							<div class="col_one_third col_last">
								<label for="template-contactform-phone">Telefon</label>
								<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_two_third">
								<label for="template-contactform-subject">Konu <small>*</small></label>
								<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label for="template-contactform-message">Mesaj <small>*</small></label>
								<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
							</div>

							<div class="col_full hidden">
								<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
							</div>

							<div class="col_full">
								<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Gönder</button>
							</div>

						</form>
					</div>

				</div><!-- .postcontent end -->

				<!-- Sidebar
                ============================================= -->
				<div class="sidebar col_last nobottommargin">

					<address>
						<strong>ADRES:</strong><br>
						{{$setting->address}}
					</address>
					<strong>Tel:</strong> {{$setting->phone}}<br>
					<strong>Fax:</strong> {{$setting->fax}}<br>
					<strong>Email:</strong> {{$setting->email}}

					<div class="widget noborder notoppadding" style="margin-top:10px;">

						<a href="#" class="social-icon si-small si-dark si-facebook">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>

						<a href="#" class="social-icon si-small si-dark si-twitter">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>
						<a href="#" class="social-icon si-small si-dark si-instagram">
							<i class="icon-instagram"></i>
							<i class="icon-instagram"></i>
						</a>
						<br>
						<hr>

						<h4 class="heading-primary"><strong>ÇALIŞMA SAATLERİMİZ</strong></h4>
						<table class="table table-responsive">
							<tbody>
								<tr>
									<td>Pazartesi</td>
									<td><i class="fa fa-clock-o"></i></td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Salı</td>
									<td><i class="fa fa-clock-o"></i></td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Çarşamba</td>
									<td><i class="fa fa-clock-o"></i></td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Perşembe</td>
									<td><i class="fa fa-clock-o"></i></td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Cuma</td>
									<td><i class="fa fa-clock-o"></i></td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Cumartesi</td>
									<td><i class="fa fa-clock-o"></i></td>
									<td>09:00 - 18:00</td>
								</tr>
							</tbody>
						</table>


					</div>

				</div><!-- .sidebar end -->

			</div>

		</div>

	</section><!-- #content end -->

@endsection
