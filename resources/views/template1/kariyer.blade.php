@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack"></div>

    <div class="container clearfix">
        <h1>KARİYER</h1>
        <span></span>
        <ol class="breadcrumb">
            <li><a href="#">Anasayfa</a></li>
            <li class="active">Kariyer</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="container clearfix">

      <div class="row clearfix" style="margin-top: -80px;">
        <div class="col-md-12">
          <div class="heading-block nobottomborder bottommargin-sm">
            <span style="font-size:18px; color:#000; font-weight:bold;">Bu sayfa yapım aşamasındadır. İlgiliniz için teşekkür ederiz.</h4>            
          </div>
        </div>
      </div>
    </div>

  </div>

</section><!-- #content end -->

@endsection
