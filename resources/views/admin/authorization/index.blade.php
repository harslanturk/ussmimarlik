@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
   Yetki Grupları
    <small>Modüller Listesi</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><a href="/admin/authorization"><i class="fa fa-dashboard active"></i> Yetki Grupları</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary" style="padding: 1%;">
        <div class="col-sm-5 pull-right" style="padding: 1%;">
          <form action="/admin/authorization/createAuthGroup" method="post">
            {{ csrf_field() }}
            <table style="width:100%;">
              <tr>
                <td><b>Yeni Yetki Grubu Ekle</b></td>
                <td>
                  <div class="input-group input-group-sm pull-right">
                    <input type="text" class="form-control" name="name" placeholder="Yetki Grubu Ekle" style="width:100%;color:red;">
                    <span class="input-group-btn">
                      <button class="btn btn-info btn-flat" type="submit">Kaydet</button>
                    </span>
                  </div><!-- /input-group -->
                </td>
              </tr>
            </table>
          </form>
        </div>
			<div class="box-header">
				<h3 class="box-title"></h3>
        <form class="form-group">
          {{ csrf_field() }}
              <table id="authorization-table" class="table table-bordered table-hover table-striped">
                <thead>
                  <tr>
                    <th>Sıra</th>
                    <th>Yetki Grubu</th>
                    <th class="col-sm-2"></th>
                  </tr>
                </thead>
                <tbody>
                <?php $sira=1; ?>
                    @foreach($auth_groups as $auth_group)
                  <tr>
                    <td>{{ $sira }}</td>
                    <td>{{$auth_group->name}}</td>
                    <td>
                        <a class="button btn btn-primary" title="Güncelle" href="/admin/authorization/getauthorization/{{ $auth_group->id }}"><i class="fa fa-edit"></i></a>
                      @if($auth_group->name != 'Admin')
                        <a class="button btn btn-danger" title="Sil" onclick="deleteApprove('/admin/authorization/deleteAuthGroup/{{ $auth_group->id }}')"><i class="fa fa-trash"></i></a>
                      @endif
                    </td>
                  </tr>
                  <?php $sira++; ?>
                  @endforeach
                </tbody>
              </table>
        </select>
        </div>
        </form>
			</div>
			</div>

		</div>
	</div>
</section><!-- /.content -->

</div><!-- /.content-wrapper -->
@endsection
