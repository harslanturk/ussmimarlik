@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- ALERT -->
      @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {{ Session::get('flash_notification.message') }}
          </div>
      @endif
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kampanyalar
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><a href="/admin/campaign"><i class="fa fa-dashboard active"></i> Kampanyalar</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  @if(App\Helpers\helper::authControl('kampanya','add'))
    <a href="#" class="button btn btn-primary" data-toggle="modal" data-target="#modalYeniKampanya">Yeni Kampanya Ekle</a>
  @endif
	<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Kampanya Listesi</h3>
		</div><!-- /.box-header -->
		<div class="box-body no-padding">
		  <table id="campaign_table" class="table table-bordered table-striped table-hover">
		    <thead>
		    <tr>
		      <th>#</th>
		      <th>Başlık</th>
		      <th>İçerik</th>
		      <th>Tür</th>
		      <th class="col-sm-2">Resim</th>
		      <th></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php
		    	$count = 0;
		    ?>
		    @foreach($campaigns as $campaign)
		    <?php
		    	$count++;
		    ?>
		    <tr>
		      <td>{{$count}}.</td>
		      <td>{{$campaign->baslik}}</td>
		      <td>{{$campaign->icerik}}</td>
		      <td>{{$campaign->tur}}</td>
          @if($campaign->resim)
              <td><img src="{{$campaign->resim}}" class="img-responsive"></td>
              @else
              <td class="text-danger"><i class="fa fa-warning"></i> Resim Yok</td>
          @endif
		      <td>
            @if(App\Helpers\helper::authControl('kampanya','update'))
		      	<a href="/admin/campaign/edit/{{$campaign->id}}" class="button btn btn-success"><i class="fa fa-edit"></i></a>
            @endif
            @if(App\Helpers\helper::authControl('kampanya','delete'))
				    <a class="button btn btn-danger" onclick="deleteApprove('/admin/campaign/delete/{{$campaign->id}}')"><i class="fa fa-trash"></i></a>
            @endif
		      </td>
		    </tr>
		    @endforeach
		    </tbody>
		  </table>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- Randevu Ekleme Modeli -->
<div class="modal fade" id="modalYeniKampanya" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form role="form" action="{{URL::to('/admin/campaign/create')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Yeni Kampanya Ekle</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Başlık</label>
                            <input class="form-control" type="text" name="baslik">
                        </div>
                        <div class="form-group">
                            <label>İçerik</label>
                            <textarea name="icerik" class="form-control" rows="8" cols="80" style="resize:none;"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Deri Adı</label>
                            <select class="select2 form-control" name="tur" style="width:100%;">
                              <option value="Uçak Kampanyası">Uçak Kampanyası</option>
                              <option value="Otel Kampanyası">Otel Kampanyası</option>
                            </select>
                        </div>
                        <div class="form-group">
                        <div class="pull-left">
                            <div class="btn btn-warning btn-file">
                                <i class="fa fa-image"></i> Resim Seç
                                <input class="btn" type="file" name="resim">
                            </div>
                        </div>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kampanya Ekle</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div>
@endsection
