@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kampanyalar
    <small>Kampanya Güncelleme</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><a href="/admin/campaign"><i class="fa fa-dashboard active"></i> Kampanyalar</a></li>
  </ol>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"> Kampanya Düzenle</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
        <form action="/admin/campaign/save" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
          <div role="form">
            <div class="box-body">
              <div class="form-group">
                  <label>Başlık</label>
                  <input type="text" class="form-control" name="baslik" placeholder="Başlık" value="{{$campaign->baslik}}">
              </div>
              <div class="form-group">
                  <label>İçerik</label>
                  <textarea name="icerik" class="form-control" rows="8" cols="80" style="resize:none;">{{$campaign->icerik}}</textarea>
              </div>
              <div class="form-group">
                  <label>Tür</label>
                  <select class="select2 form-control" name="tur" style="width:100%;">
                    <option value="Uçak Kampanyası" <?php if($campaign->tur == 'Uçak Kampanyası') { echo 'selected';}?>>Uçak Kampanyası</option>
                    <option value="Otel Kampanyası" <?php if($campaign->tur == 'Otel Kampanyası') { echo 'selected';}?>>Otel Kampanyası</option>
                  </select>
              </div>
              <div class="form-group">
                @if(!$campaign->resim)
                <div class="pull-left">
                    <div class="btn btn-warning btn-file">
                        <i class="fa fa-image"></i> Resim Seç
                        <input class="btn" type="file" name="image" id="image">
                    </div>
                </div>
                    @else
                <div class="pull-left">
                    <a href="{{URL::to('/admin/campaign/imageDelete/'.$campaign->id)}}" class="btn btn-danger" title="Resmi Sil"><i class="fa fa-trash"></i></a>
                    <div class="col-sm-6">
                        <img class="img-responsive img-bordered" src="{{URL::to($campaign->resim)}}" alt="User profile picture">
                    </div>
                </div>
                @endif
              </div>
            </div><!-- /.box-body -->
            <input type="hidden" name="id" value="{{$campaign->id}}">
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Güncelle </button>
            </div>
          </div>
        </form>
        </div><!-- /.box -->
      </div><!--/.col (left) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection
