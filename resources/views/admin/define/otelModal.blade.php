<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/define/otelSave')}}" method="POST">
        {{ csrf_field() }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Otel Güncelle</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                  <div class="form-group">
                    <select class="select2" name="lokasyon_id" id="lokasyon_id" style="width:100%;">
                      <option value="0" disabled selected>Lokasyon Seçiniz</option>
                      @foreach($lokasyon as $lok)
                      @if($lok->id == $otel->lokasyon_id)
                      <option selected value="{{$lok->id}}">{{$lok->name}}</option>
                      @else
                      <option value="{{$lok->id}}">{{$lok->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                    <div class="form-group">
                        <label>Otel İşlemi</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{$otel->name}}">
                        <input type="hidden" name="id" value="{{$otel->id}}">
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-success">Güncelle</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
