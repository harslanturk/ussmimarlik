@extends('admin.master')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tanımlamalar
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li class="active">Tanımlamalar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tanımlamalar</h3>
              <div class="box-tools">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="/admin/define"><i class="fa fa-inbox"></i> Lokasyonlar</a></li>
                <li><a href="/admin/define/otel"><i class="fa fa-bed"></i> Oteller</a></li>
                <li><a href="/admin/define/hava-yolu"><i class="fa fa-plane"></i> Hava Yolu</a></li>
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /. box -->
        </div><!-- /.col -->
        <div class="col-md-9">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Lokasyon Tanımlamaları</h3>
              </div><!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tr>
                    @if(App\Helpers\helper::authControl('tanimlamalar','add'))
                    <td colspan="3">
                      <form action="/admin/define/lokasyonEkle" method="post">
                        {{ csrf_field() }}
                        <table style="width:100%;">
                          <tr>
                            <td><b>Yeni Lokasyon Ekle</b></td>
                            <td>
                              <div class="input-group input-group-sm pull-right">
                                <input type="text" class="form-control" name="name" placeholder="Lokasyon Ekle" style="width:200px;">
                                <span class="input-group-btn">
                                  <button class="btn btn-info btn-flat" type="submit">Kaydet</button>
                                </span>
                              </div><!-- /input-group -->
                            </td>
                          </tr>
                        </table>
                      </form>
                    </td>
                    @endif
                  </tr>
                  <tr>
                    <th>ID</th>
                    <th>Lokasyon Adı</th>
                    <th>#</th>
                  </tr>
                  @foreach($lokasyon as $kont)
                  <tr>
                    <td>{{ $kont->id }}</td>
                    <td>{{ $kont->name }}</td>
                    <td>
                      @if(App\Helpers\helper::authControl('tanimlamalar','update'))
                      <a href="/admin/define/lokasyonDelete/{{ $kont->id }}" onclick="return silOnayla();">
                        <i class="glyphicon glyphicon-remove" style="color:red;"></i>
                      </a>
                      @endif
                      @if(App\Helpers\helper::authControl('tanimlamalar','delete'))
                      <a href="#" class="LokasyonGuncelle" id="{{ $kont->id }}" style="margin:0 0 0 3px;" data-toggle="modal" data-target="#modalLokasyonGuncelle">
                        <i class="fa fa-external-link text-green"></i>
                      </a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <!-- EngelTipGuncelle Modal -->
  <div class="modal fade" id="modalLokasyonGuncelle" tabindex="-1" role="dialog">

  </div><!-- /.modal -->
  <script type="text/javascript">
      $(document).ready(function () {

          $('.LokasyonGuncelle').click(function () {
              var lokasyon_id = $(this).attr('id');
              //console.log(engel_id);
              $.ajax({
                  url: '/admin/define/lokasyon-edit',
                  type: 'POST',
                  beforeSend: function (xhr) {
                      var token = $('meta[name="csrf_token"]').attr('content');

                      if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                      }
                  },
                  cache: false,
                  data: {lokasyon_id: lokasyon_id},
                  success: function(data){
                      document.getElementById('modalLokasyonGuncelle').innerHTML=data;
                  },
                  error: function(jqXHR, textStatus, err){}
              });
          });
      });
  </script>
@stop()
