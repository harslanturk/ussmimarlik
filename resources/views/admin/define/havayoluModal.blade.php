<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/define/havayoluSave')}}" method="POST">
        {{ csrf_field() }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Havayolu Güncelle</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label>Havayolu İşlemi</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{$havayolu->name}}">
                        <input type="hidden" name="id" value="{{$havayolu->id}}">
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-success">Güncelle</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
