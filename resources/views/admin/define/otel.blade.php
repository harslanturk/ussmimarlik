@extends('admin.master')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tanımlamalar
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li class="active">Tanımlamalar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tanımlamalar</h3>
              <div class="box-tools">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/admin/define"><i class="fa fa-inbox"></i> Lokasyonlar</a></li>
                <li class="active"><a href="/admin/define/otel"><i class="fa fa-bed"></i> Oteller</a></li>
                <li><a href="/admin/define/hava-yolu"><i class="fa fa-plane"></i> Hava Yolu</a></li>
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /. box -->
        </div><!-- /.col -->
        <div class="col-md-9">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Otel Tanımlamaları</h3>
              </div><!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tr>
                    @if(App\Helpers\helper::authControl('tanimlamalar','add'))
                    <td colspan="3">
                      <form action="/admin/define/otelEkle" method="post">
                        {{ csrf_field() }}
                        <table style="width:100%;">
                          <tr>
                            <td><b>Yeni Otel Ekle</b></td>
                            <td>
                              <select class="select2" name="lokasyon_id" id="lokasyon_id" style="width:100%;">
                                <option value="0" disabled selected>Lokasyon Seçiniz</option>
                                @foreach($lokasyon as $lok)
                                <option value="{{$lok->id}}">{{$lok->name}}</option>
                                @endforeach
                              </select>
                            </td>
                            <td>
                              <div class="input-group input-group-sm pull-right">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Otel Ekle" style="width:200px;">
                                <span class="input-group-btn">
                                  <button class="btn btn-info btn-flat" type="submit" id="kaydet">Kaydet</button>
                                </span>
                              </div><!-- /input-group -->
                            </td>
                          </tr>
                        </table>
                      </form>
                    </td>
                    @endif
                  </tr>
                  <tr>
                    <th>ID</th>
                    <th>Otel Adı</th>
                    <th>#</th>
                  </tr>
                  @foreach($otels as $otel)
                  <tr>
                    <td>{{ $otel->id }}</td>
                    <td>{{ $otel->name }}</td>
                    <td>
                      @if(App\Helpers\helper::authControl('tanimlamalar','update'))
                      <a href="/admin/define/otelDelete/{{ $otel->id }}" onclick="return silOnayla();">
                        <i class="glyphicon glyphicon-remove" style="color:red;"></i>
                      </a>
                      @endif
                      @if(App\Helpers\helper::authControl('tanimlamalar','delete'))
                      <a href="#" class="OtelGuncelle" id="{{ $otel->id }}" style="margin:0 0 0 3px;" data-toggle="modal" data-target="#modalOtelGuncelle">
                        <i class="fa fa-external-link text-green"></i>
                      </a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <!-- EngelTipGuncelle Modal -->
  <div class="modal fade" id="modalOtelGuncelle" tabindex="-1" role="dialog">

  </div><!-- /.modal -->
  <script type="text/javascript">
      $(document).ready(function () {
        document.getElementById('name').disabled=true;
        document.getElementById('kaydet').disabled=true;
          $('.OtelGuncelle').click(function () {
              var otel_id = $(this).attr('id');
              //console.log(engel_id);
              $.ajax({
                  url: '/admin/define/otel-edit',
                  type: 'POST',
                  beforeSend: function (xhr) {
                      var token = $('meta[name="csrf_token"]').attr('content');

                      if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                      }
                  },
                  cache: false,
                  data: {otel_id: otel_id},
                  success: function(data){
                      document.getElementById('modalOtelGuncelle').innerHTML=data;
                      $(".select2").select2({
                        autocomplete: true
                      });
                  },
                  error: function(jqXHR, textStatus, err){}
              });
          });
          $('#lokasyon_id').change(function () {
              document.getElementById('name').disabled=false;
              document.getElementById('kaydet').disabled=false;
          });
      });
  </script>
@stop()
