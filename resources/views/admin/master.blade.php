<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <title><?php
      $setting = App\Setting::where('id',1)->first();
      echo $setting->name;
    ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/AdminLTE.css">
    <link rel="stylesheet" href="/css/main.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/plugins/select2/select2.min.css">
    <!-- Sweet Alert -->
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/dist/sweetalert.css">
    <!-- Select2 -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.1.4 -->
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Slimscroll -->
    <script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="{{URL::to('/admin')}}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?php echo substr($setting->name,0,3); ?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?php echo $setting->name; ?></b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">


              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="/img/avatar.png" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ $user=Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="/img/avatar.png" class="img-circle" alt="User Image">

                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="/admin/users/edit/{{ Auth::user()->id }}" class="btn btn-default btn-flat">Profil</a>
                    </div>
                    <div class="pull-right">
                      <a href="/logout" class="btn btn-default btn-flat">Çıkış</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="/img/avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
            <div class="input-group">
              <input type="text" name="q" class="form-control notificationLink">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat notificationLink"><i class="fa fa-search"></i></button>
              </span>
            </div>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENÜ</li>
            <?php
              $auths = App\Helpers\helper::getAuthParent(Auth::user()->id);
              $subParent = App\Helpers\helper::getAuthSubParent(Auth::user()->id);
              //print_r($auths);
            ?>
            @foreach($auths as $auth)
              <?php

              if($auth->read==1){
                if($auth->parent==1){
                  echo ' <li class="treeview">
                <a href="#">
                  <i class="fa '.$auth->icon.'"></i>
                  <span>'.$auth->name.'</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                ';
                  foreach ($subParent as $val) {
                    if($val->read==1 && $val->parent_id==$auth->id){
                      echo '<li><a href="'.$val->url.'"><i class="fa '.$val->icon.'"></i> '.$val->name.'</a></li>';
                    }
                  }
                  echo '
                </ul>
              </li>';
                }else{
                  echo '<li class="treeview">
                  <a href="'.$auth->url.'">
                    <i class="fa '.$auth->icon.'"></i> <span>'.$auth->name.'</span> </a>

              </li>';}

              }
              ?>

            @endforeach
            <!--<li class="active treeview">
              <a href="/">
                <i class="fa fa-dashboard"></i> <span>Anasayfa</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Öğrenci</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/tum-ogrenciler"><i class="fa fa-circle-o"></i> Tüm Öğrenciler</a></li>
                <li><a href="/ogrenci-ekle"><i class="fa fa-circle-o"></i> Öğrenci Ekle</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Personel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/tum-personeller"><i class="fa fa-users"></i> Tüm Personeller</a></li>
                <li><a href="/personel-ekle"><i class="fa fa-user-plus"></i> Personel Ekle</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-info"></i>
                <span>Tanımlamalar</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/ogrenci-islemleri"><i class="fa fa-info"></i> Öğrenci İşlemleri</a></li>
                <li><a href="/engel-tipleri"><i class="fa fa-user-plus"></i> Engel Tipleri</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-plus"></i>
                <span>Yetkilendirme İşlemleri</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-comment-o"></i>
                <span>Mesajlar</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-comment-o"></i> Gelen Kutusu</a></li>
                <li><a href="#"><i class="fa fa-comment-o"></i> Giden Kutusu</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-comment-o"></i>
                <span>SMS Modülü</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-comment-o"></i> Hızlı SMS Gönder</a></li>
                <li><a href="#"><i class="fa fa-comment-o"></i> SMS Şablonları</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-question"></i>
                <span>Danışman Modülü</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-question"></i>
                <span>Destek</span>
              </a>
            </li>-->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      @yield('content')
      <footer class="main-footer none-print">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.2
        </div>
        <strong>Copyright &copy; 2016 <a href="http://cozumlazim.com">ÇözümLazım.com</a>.</strong> Tüm Hakları Saklıdır.
      </footer>


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="/plugins/select2/select2.full.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- DataTables -->
    <script src="/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- InputMask -->
    <script src="/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- FastClick -->
    <script src="/plugins/fastclick/fastclick.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="/plugins/chartjs/Chart.min.js"></script>
    <!--AJAX-->
    <script src="/js/ajax.js"></script>
    <script src="/js/custom.js"></script>
    <!-- AdminLTE App -->
    <script src="/js/app.js"></script>
    <!-- Sweet Alert -->
    <script src="/plugins/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        $('.image_control').change(function(){
              var file = document.getElementsByClassName('image_control');
              console.log(file[0].files[0].size);
              if (file[0].files[0].size > 2000000) {
                alert('Fotoğraf Boyutu En Fazla 2Mb Olabilir !');
                file[0].value = "";
              }
        });
        if(window.location.pathname.split('/')[2] == 'sms'){
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_secerek').value.length;
            document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            var kalan_karakter_2 = 160 - document.getElementById('sms_mesaj_elle').value.length;
            document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter_2;
            var kalan_karakter_1 = 160 - document.getElementById('sms_mesaj_personel').value.length;
            document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter_1;
        }
        function islem_gonder_elle() {
            var kalan_karakter = 160 - document.getElementById('islem_mesaj_secerek').value.length;
            document.getElementById('islem_kalan_karakter').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('islem_kalan_karakter').innerHTML = kalan_karakter;
            }else{
                document.getElementById('islem_mesaj_secerek').value = document.getElementById('islem_mesaj_secerek').value.substring(0,160);
                console.log('substr')
                document.getElementById('islem_kalan_karakter').innerHTML = kalan_karakter;
            }
        }
        function sms_gonder_elle() {
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_secerek').value.length;
            document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            }else{
                document.getElementById('sms_mesaj_secerek').value = document.getElementById('sms_mesaj_secerek').value.substring(0,160);
                console.log('substr')
                document.getElementById('kalan_karakter').innerHTML = kalan_karakter;
            }
        }
        function sms_gonder_elle_2() {
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_elle').value.length;
            document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter;
            }else{
                document.getElementById('sms_mesaj_elle').value = document.getElementById('sms_mesaj_elle').value.substring(0,160);
                console.log('substr')
                document.getElementById('kalan_karakter_2').innerHTML = kalan_karakter;
            }
        }function sms_gonder_elle_3() {
            var kalan_karakter = 160 - document.getElementById('sms_mesaj_personel').value.length;
            document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter;
            if(kalan_karakter > 0){
                document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter;
            }else{
                document.getElementById('sms_mesaj_personel').value = document.getElementById('sms_mesaj_personel').value.substring(0,160);
                console.log('substr')
                document.getElementById('kalan_karakter_1').innerHTML = kalan_karakter;
            }
        }
      /*$(window).load(function(){
        $('#modalIlkMesaj').modal('show');
      });*/
      $("#calendar").datepicker({
        language: 'tr'
      });
      $("[data-mask]").inputmask();
        function addStudent()
         {
            var inputOgrenciNo = document.getElementById('inputOgrenciNo').value;
            var inputTcNo = document.getElementById('inputTcNo').value;
            var inputEngelTipi = document.getElementById('inputEngelTipi').value;
            var inputAdi = document.getElementById('inputAdi').value;
            var inputSoyadi = document.getElementById('inputSoyadi').value;
            var inputCinsiyeti = document.getElementById('inputCinsiyeti').value;
            var inputDogumTarihi = document.getElementById('inputDogumTarihi').value;
            var inputDogumYeri = document.getElementById('inputDogumYeri').value;
            var inputOkulunAdi = document.getElementById('inputOkulunAdi').value;
            var inputBabaAdi = document.getElementById('inputBabaAdi').value;
            var inputBabaTel = document.getElementById('inputBabaTel').value;
            var inputKayitTarihi = document.getElementById('inputKayitTarihi').value;
            var inputAnneAdi = document.getElementById('inputAnneAdi').value;
            var inputAnneTel = document.getElementById('inputAnneTel').value;
            var inputAyrilisTarihi = document.getElementById('inputAyrilisTarihi').value;
            var inputEgitimSekli = document.getElementById('inputEgitimSekli').value;
            var inputDigerTel = document.getElementById('inputDigerTel').value;
            var inputIl = document.getElementById('inputIl').value;
            var inputIlce = document.getElementById('inputIlce').value;
            var inputAdres = document.getElementById('inputAdres').value;
            $.ajax({
              url: '/admin/students/new',
              type: 'POST',
              beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
              cache: false,
              data: {inputOgrenciNo: inputOgrenciNo, inputTcNo:  inputTcNo, inputEngelTipi:  inputEngelTipi, inputAdi:  inputAdi, inputSoyadi:  inputSoyadi, inputCinsiyeti:  inputCinsiyeti, inputDogumTarihi:  inputDogumTarihi, inputDogumYeri:  inputDogumYeri, inputOkulunAdi:  inputOkulunAdi, inputBabaAdi:  inputBabaAdi, inputBabaTel:  inputBabaTel, inputKayitTarihi:  inputKayitTarihi, inputAnneAdi:  inputAnneAdi, inputAnneTel:  inputAnneTel, inputAyrilisTarihi:  inputAyrilisTarihi, inputEgitimSekli:  inputEgitimSekli, inputDigerTel:  inputDigerTel, inputIl:  inputIl, inputIlce:  inputIlce, inputAdres:  inputAdres },
              success: function(data){
              location.href = "/";
              alert("Öğrenci Başarıyla Eklendi");
              },
              error: function(jqXHR, textStatus, err){}
            });
         }
      function saveStudentImage()
      {
        var inputPresim = document.getElementById('profilResim').value;
        var inputPresimOgrenciId = document.getElementById('presim_ogrenci_id').value;
        $.ajax({
          url: '/admin/students/update-resim',
          type: 'POST',
          beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
          },
          cache: false,
          data: {ogrenci_id: inputPresimOgrenciId, resim: inputPresim },
          success: function(data){
            /*location.href = "/";
            location.reload();
            alert("Öğrenci Resim Başarıyla Eklendi");*/
          },
          error: function(jqXHR, textStatus, err){}
        });
      }
        function saveStudent()
         {
            var inputOgrenciId = document.getElementById('inputOgrenciId').value;
            var inputOgrenciNo = document.getElementById('inputOgrenciNo').value;
            var inputTcNo = document.getElementById('inputTcNo').value;
            var inputEngelTipi = document.getElementById('inputEngelTipi').value;
            var inputAdi = document.getElementById('inputAdi').value;
            var inputSoyadi = document.getElementById('inputSoyadi').value;
            var inputCinsiyeti = document.getElementById('inputCinsiyeti').value;
            var inputDogumTarihi = document.getElementById('inputDogumTarihi').value;
            var inputDogumYeri = document.getElementById('inputDogumYeri').value;
            var inputOkulunAdi = document.getElementById('inputOkulunAdi').value;
            var inputBabaAdi = document.getElementById('inputBabaAdi').value;
            var inputBabaTel = document.getElementById('inputBabaTel').value;
            var inputKayitTarihi = document.getElementById('inputKayitTarihi').value;
            var inputAnneAdi = document.getElementById('inputAnneAdi').value;
            var inputAnneTel = document.getElementById('inputAnneTel').value;
            var inputAyrilisTarihi = document.getElementById('inputAyrilisTarihi').value;
            var inputEgitimSekli = document.getElementById('inputEgitimSekli').value;
            var inputDigerTel = document.getElementById('inputDigerTel').value;
            var inputIl = document.getElementById('inputIl').value;
            var inputIlce = document.getElementById('inputIlce').value;
            var inputAdres = document.getElementById('inputAdres').value;
            $.ajax({
              url: '/admin/students/update',
              type: 'POST',
              beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
              cache: false,
              data: {id: inputOgrenciId, ogrenci_no: inputOgrenciNo, tcno:  inputTcNo, engel_tipi:  inputEngelTipi, ad:  inputAdi, soyad:  inputSoyadi, cinsiyet:  inputCinsiyeti, dogum_tarihi:  inputDogumTarihi, dogum_yeri:  inputDogumYeri, okul:  inputOkulunAdi, baba:  inputBabaAdi, baba_tel:  inputBabaTel, kayit_tarihi:  inputKayitTarihi, anne:  inputAnneAdi, anne_tel:  inputAnneTel, ayrilis_tarihi:  inputAyrilisTarihi, egitim:  inputEgitimSekli, diger_tel:  inputDigerTel, il:  inputIl, ilce:  inputIlce, adres:  inputAdres },
              success: function(data){
              /*location.href = "/";*/
                location.reload();
              alert("Öğrenci Başarıyla Eklendi");
              },
              error: function(jqXHR, textStatus, err){}
            });
         }
        function addPersonel()
         {
            var inputYetkiGrubu = document.getElementById('inputYetkiGrubu').value;
            var inputTcNo = document.getElementById('inputTcNo').value;
            var inputName = document.getElementById('inputName').value;
            var inputCinsiyeti = document.getElementById('inputCinsiyeti').value;
            var inputDogumTarihi = document.getElementById('inputDogumTarihi').value;
            var inputDogumYeri = document.getElementById('inputDogumYeri').value;
            var inputBabaAdi = document.getElementById('inputBabaAdi').value;
            var inputKayitTarihi = document.getElementById('inputKayitTarihi').value;
            var inputAnneAdi = document.getElementById('inputAnneAdi').value;
            var inputAyrilisTarihi = document.getElementById('inputAyrilisTarihi').value;
            var inputTelefon = document.getElementById('inputTelefon').value;
            var inputIsTelefon = document.getElementById('inputIsTelefon').value;
            var inputDigerTelefon = document.getElementById('inputDigerTelefon').value;
            var inputIl = document.getElementById('inputIl').value;
            var inputIlce = document.getElementById('inputIlce').value;
            var inputAdres = document.getElementById('inputAdres').value;
            var inputEmail = document.getElementById('inputEmail').value;
            var inputPass = document.getElementById('inputPass').value;
            $.ajax({
              url: '/admin/staff/new',
              type: 'POST',
              beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
              cache: false,
              data: {password: inputPass,email: inputEmail,delegation_id: inputYetkiGrubu, tcno:  inputTcNo, name:  inputName, cinsiyet:  inputCinsiyeti, dogum_tarihi:  inputDogumTarihi, dogum_yeri:  inputDogumYeri, baba:  inputBabaAdi, kayit_tarihi:  inputKayitTarihi, anne:  inputAnneAdi, ayrilis_tarihi:  inputAyrilisTarihi, telefon:  inputTelefon, is_telefon:  inputIsTelefon, diger_telefon:  inputDigerTelefon, il:  inputIl, ilce:  inputIlce, adres:  inputAdres },
              success: function(data){
              location.href = "/";
              alert("Personel Başarıyla Eklendi");
              },
              error: function(jqXHR, textStatus, err){}
            });
         }
         $("#users_table, #intro_table, #popup_table, #pagein_table,#hastalar_table,#campaign_table,#authorization-table,#slider_table").DataTable({
             "language": {
                 "emptyTable": "Hiç bir veri bulunamadı",
                 "info": "Gösterim _START_ ile _END_ arası _TOTAL_ toplam veri",
                 "infoEmpty": "Gösterim 0 ile 0 arası 0 toplam veri",
                 "infoFiltered": "(filtrelenen _MAX_ toplam veri)",
                 "lengthMenu": "Gösterim _MENU_ veri",
                 "loadingRecords": "Yükleniyor...",
                 "processing": "İşleniyor...",
                 "search": "Arama:",
                 "zeroRecords": "Eşleşen kayıt bulunamadı.",
                 "paginate": {
                     "first": "İlk",
                     "last": "Son",
                     "next": "İleri",
                     "previous": "Geri"
                 },
                 "aria": {
                     "sortAscending":  ": aktif sıralama azdan çok",
                     "sortDescending": ": aktif sıralama çoktan az"
                 }
             }
         });
        $(function () {
          $('#ogrenci-table,#delegationtable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true
          });
        });
      $(function () {
        //Add text editor
        $("#compose-textarea").wysihtml5();
      });
      $(".select2").select2({
        autocomplete: true
      });
      function silOnayla()
      {
        return confirm("Silmek istediğinizden emin misiniz?");
      }
      function deleteApprove(link) {
        swal({
                  title: "Silmek istediğinize emin misiniz?",
                  text: "Eğer silerseniz, sildiğiniz veriye bir daha ulaşamayacağınızı onaylamış olursunuz!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, veriyi sil!",
                  cancelButtonText: "Hayır, veriyi silme!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Silindi!", "Veriniz başarılı bir şekilde silindi.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "Verinize herhangi bir işlem yapılmadı.", "error");
                  }
                });
      }
      function deleteApproveStudent(link,konu) {
        if(konu > 0){
        swal({
                  title: "Öğrencinin Bitmemiş Konusu Bulunmakta Yinede Silmek İstiyormusunuz!",
                  text: "Eğer silerseniz, sildiğiniz veriye bir daha ulaşamayacağınızı onaylamış olursunuz!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, veriyi sil!",
                  cancelButtonText: "Hayır, veriyi silme!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Silindi!", "Veriniz başarılı bir şekilde silindi.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "Verinize herhangi bir işlem yapılmadı.", "error");
                  }
                });
                }
        else{
          swal({
                    title: "Silmek istediğinize emin misiniz?",
                    text: "Eğer silerseniz, sildiğiniz veriye bir daha ulaşamayacağınızı onaylamış olursunuz!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Evet, veriyi sil!",
                    cancelButtonText: "Hayır, veriyi silme!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                  },
                  function(isConfirm){
                    if (isConfirm) {
                      swal("Silindi!", "Veriniz başarılı bir şekilde silindi.", "success");
                      location.href = link;
                    } else {
                      swal("İptal Edildi", "Verinize herhangi bir işlem yapılmadı.", "error");
                    }
                  });
        }
      }
      function readApprove(link) {
        swal({
                  title: "Okundu durumunu değiştirmek istediğinize emin misiniz?",
                  text: "Eğer devam ederseniz, okundu / okunmadı olarak değiştirilecektir.",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, devam et!",
                  cancelButtonText: "Hayır, iptal!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Değiştirildi!", "Danışman notu başarılı bir şekilde değiştirildi.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "İşleminiz iptal edildi.", "error");
                  }
                });
      }
      function konuOpenApprove(link) {
        swal({
                  title: "Bu konuyu açmak istediğinize emin misiniz?",
                  text: "Eğer bu konuyu açarsanız, diğer kullanıcılar yeni işlem ekleyebilirler!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Evet, bu konuyu aç!",
                  cancelButtonText: "Hayır, bu konuyu açma!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {
                    swal("Konu Açıldı!", "Konunuz başarılı bir şekilde açıldı.", "success");
                    location.href = link;
                  } else {
                    swal("İptal Edildi", "Konunuz açılmamıştır.", "error");
                  }
                });
      }
      function getSearch()
       {
          var query = document.getElementById('notificationLink').value;
          var say=query.length;
          if(say>"1")
          {
          $.ajax({
            url: '/admin/students/search',
            type: 'POST',
            beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
            cache: false,
            data: {query: query},
            success: function(data){
              document.getElementById('notificationsBody').innerHTML=data;
            },
            error: function(jqXHR, textStatus, err){}
         });
          }
       }
       /* Notifikasyon Ekranı */
        $(".notificationLink").click(function()
        {
       //okundu işlemi burda yapılabilir.
            $("#notificationContainer").fadeToggle(300);
            $("#notification_count").fadeOut("slow");
            return false;
        });
     //Document Click
        $(document).click(function()
        {
            $("#notificationContainer").hide();
        });
      //Popup Click
        $("#notificationContainer").click(function()
        {
            //return false
        });

        /* Notifikasyon Ekranı */
    </script>
    @yield('edit-js')
  </body>
</html>
