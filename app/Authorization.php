<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authorization extends Model
{
    protected $table = 'authorization';
    protected $fillable = [
      'user_id','modul_id','group_id','read','update','delete','add'
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
