<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otel extends Model
{
    protected $table = 'otel';
    protected $fillable = [
      'lokasyon_id','name','status'
    ];

    public function lokasyon()
    {
      return $this->belongsTo('App\Lokasyon');
    }
}
