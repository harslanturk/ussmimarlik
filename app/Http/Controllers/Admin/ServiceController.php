<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;

class ServiceController extends Controller
{
    public function index(){
        /*die(Session::get('deneme'));*/
    	$services = Service::all();
    	/*$emc_blog_table = DB::getSchemaBuilder()->getColumnListing('emc_blog');
    	return view('admin.blog.index',['blogs' => $blogs, 'emc_blog_table' => $emc_blog_table]);	*/

    	return view('admin.service.index', ['services' => $services]);

    }
    public function create(){
    	return view('admin.service.create');
    }

    public function save(Request $request){
    	$hizmet = new Service();
        $hizmet->title = $request->input('title');
        $hizmet['slug']=$request->input('title');
        $hizmet->description = $request->input('description');
        $hizmet->content = $request->input('content');
        $hizmet->short_content = $request->input('short_content');
        $hizmet->icons = $request->input('icons');
        $hizmet->keywords = $request->input('keywords');
        $hizmet->priority = $request->input('priority');
        $hizmet->status = 1;
        $hizmet->save();
    	return redirect('/admin/services');
    	Flash::message('Hizmet başarılı bir şekilde eklendi.','success');
    }

    public function update(Request $request)
    {
        $hizmet = Service::find($request->input('id'));
        $hizmet->title = $request->input('title');
        $hizmet['slug']=str_slug($request->input('title'));
        $hizmet->description = $request->input('description');
        $hizmet->content = $request->input('content');
        $hizmet->short_content = $request->input('short_content');
        $hizmet->icons = $request->input('icons');
        $hizmet->keywords = $request->input('keywords');
        $hizmet->priority = $request->input('priority');
        $hizmet->status = 1;
        $hizmet->save();

        return redirect('/admin/services');
        Flash::message('Hizmet başarılı bir şekilde güncellendi.','success');
    }

    public function edit($id)
    {
        $allService = Service::all();
        $services = Service::find($id);
        return view('admin.service.edit',['services' => $services, 'allService' => $allService]);
    }



    public function delete($id) {
    	$services=Service::find($id);
    	$services->status=0;
    	$services->save();
    	return redirect()->back();
    }
}
