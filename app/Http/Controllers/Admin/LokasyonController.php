<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Lokasyon;
use App\Otel;
use App\Havayolu;

class LokasyonController extends Controller
{
    public function index()
    {
      $lokasyon = Lokasyon::where('status',1)->get();
      return view('admin.define.lokasyon',['lokasyon' => $lokasyon]);
    }

    public function LokasyonEkle(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      Lokasyon::create($data);
      return redirect()->back();
    }
    public function lokasyonEdit(Request $request)
    {
      $lokasyon = Lokasyon::where('id',$request['lokasyon_id'])->first();
      return view('admin.define.lokasyonModal',['lokasyon' => $lokasyon]);
    }
    public function lokasyonSave(Request $request)
    {
      $data = $request->all();
      Lokasyon::find($data['id'])->update($data);
      return redirect()->back();
    }
    public function lokasyonDelete($id)
    {
      Lokasyon::where('id',$id)->update(['status' => 0]);
      return redirect()->back();
    }
    public function Otelindex()
    {
      $lokasyon = Lokasyon::where('status',1)->get();
      $otels = Otel::where('status',1)->get();
      return view('admin.define.otel',['otels' => $otels,'lokasyon' => $lokasyon]);
    }

    public function otelEkle(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      Otel::create($data);
      return redirect()->back();
    }
    public function otelEdit(Request $request)
    {
      $lokasyon = Lokasyon::where('status',1)->get();
      $otel = Otel::where('id',$request['otel_id'])->first();
      return view('admin.define.otelModal',[
        'otel' => $otel,
        'lokasyon' => $lokasyon
      ]);
    }
    public function otelSave(Request $request)
    {
      $data = $request->all();
      Otel::find($data['id'])->update($data);
      return redirect()->back();
    }
    public function otelDelete($id)
    {
      Otel::where('id',$id)->update(['status' => 0]);
      return redirect()->back();
    }
    public function havayoluindex()
    {
      $havayolu = Havayolu::where('status',1)->get();
      return view('admin.define.havayolu',['havayolu' => $havayolu]);
    }
    public function havayoluEkle(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      Havayolu::create($data);
      return redirect()->back();
    }
    public function havayoluEdit(Request $request)
    {
      $havayolu = Havayolu::where('id',$request['havayolu_id'])->first();
      return view('admin.define.havayoluModal',['havayolu' => $havayolu]);
    }
    public function havayoluSave(Request $request)
    {
      $data = $request->all();
      Havayolu::find($data['id'])->update($data);
      return redirect()->back();
    }
    public function havayoluDelete($id)
    {
      Havayolu::where('id',$id)->update(['status' => 0]);
      return redirect()->back();
    }
}
