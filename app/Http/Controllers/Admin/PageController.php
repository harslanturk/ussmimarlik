<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use App\Categories;
use DB;
use Auth;
use App\Helpers\Helper;
use Session;

class PageController extends Controller
{
    public function index()
    {
        /*$page = Page::where('status', 1)->get();*/
        $page=DB::table('emc_page')
            ->leftjoin('emc_categories', 'emc_page.cat_id', '=', 'emc_categories.id')
            ->where('emc_page.status', 1)
            ->select('emc_page.*','emc_categories.title as cat_title')
            ->get();
        return view('admin.pages.index',['pages' => $page]);
    }
    public function draft()
    {
        $page=DB::table('emc_page')
            ->join('emc_categories', 'emc_page.cat_id', '=', 'emc_categories.id')
            ->where('emc_page.status', 2)
            ->select('emc_page.*','emc_categories.title as cat_title')
            ->get();
        return view('admin.pages.draft',['pages' => $page]);
    }
    public function deleted()
    {
        $page = Page::where('status', 0)->get();
        return view('admin.pages.deleted',['pages' => $page]);
    }
    public function create()
    {
        $allPage = Page::all();
        $categories = Categories::where('type', 'page')->where('status', 1)->get();
        return view('admin.pages.create', ['allPage' => $allPage, 'allCategories' => $categories]);
    }
    public function save(Request $request)
    {
        $page = new Page();
        $page->cat_id = $request->input('cat_id');
        $page->title = $request->input('title');
        $page->slug = str_slug($request->input('title'));
        $page->description = $request->input('description');
        $page->content = $request->input('content');
        $page->keyword = $request->input('keyword');
        $page->priority = $request->input('priority');
        $page->parent = $request->input('parent');
        $page->status = 1;
        $page->save();
    }
    public function update(Request $request)
    {
        $data = $request->all();
        /*echo "<pre>";
        print_r($data);
        die();*/
        $updateOrder = Page::find($request->input('pages_id'))->update($data);
        return redirect()->back();
    }
    public function edit($id)
    {
        $allCategories = Categories::where('status', 1)->get();
        $allPages = Page::where('status', 1)->where('id', '!=', $id)->get();
        $pages = Page::find($id);
        return view('admin.pages.edit',['pages' => $pages, 'allCategories' => $allCategories, 'allPages' => $allPages]);
    }
    public function delete($id)
    {
        $pages = Page::find($id);
        $pages->status = 0;
        $pages->save();
        return redirect()->back();
    }
    public function active($id)
    {
        $pages = Page::find($id);
        $pages->status = 1;
        $pages->save();
        return redirect()->back();
    }
    public function test()
    {
        $deger = DB::getSchemaBuilder()->getColumnListing('emc_categories');
        echo "<pre>";
        print_r($deger);
        die();
    }
}
