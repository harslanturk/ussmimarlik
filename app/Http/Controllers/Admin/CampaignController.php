<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Campaign;
use Illuminate\Support\Facades\File;

class CampaignController extends Controller
{
    public function index()
    {
      $campaign = Campaign::where('status',1)->get();
      return view('admin.campaign.index',[
        'campaigns' => $campaign
      ]);
    }
    public function createCampaign(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;

      $image = $request->file('resim');

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/img/upload/');
        $image->move($destinationPath,$input['imagename']);

        $data['resim'] = '/img/upload/'.$input['imagename'];

        Campaign::create($data);
        return redirect()->back();
    }
    public function editCampaign($id)
    {
      $campaign = Campaign::where('id',$id)->first();
      return view('admin.campaign.edit',[
        'campaign' => $campaign
      ]);
    }
    public function saveCampaign(Request $request)
    {
      $data = $request->all();

      $image = $request->file('image');
      if ($image) {
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/img/upload/');
        $image->move($destinationPath,$input['imagename']);

        $data['resim'] = '/img/upload/'.$input['imagename'];

        Campaign::find($data['id'])->update($data);
        return redirect()->back();
      }
      Campaign::find($data['id'])->update($data);
      return redirect()->back();
    }
    public function imageDelete($id)
    {
        $image = Campaign::where('id',$id)->first();

        if(File::exists(public_path($image->resim))){
            File::delete(public_path($image->resim));
            Campaign::find($id)->update(['resim'=>'']);
            return redirect()->back();
        }
    }
    public function deleteCampaign($id)
    {
      Campaign::where('id',$id)->update(['status' => 0]);
      return redirect()->back();
    }
}
