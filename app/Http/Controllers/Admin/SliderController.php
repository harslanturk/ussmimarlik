<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Slider;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;

class SliderController extends Controller
{
    public function index(){

    	$slider = Slider::all();

    	return view('admin.slider.index', ['slider' => $slider]);
    }

    public function create(){
        return view('admin.slider.create');
    }

    public function save(Request $request){

       $path = base_path() . '/public/img/slider';

       $imageTempName = $request->file('image')->getPathname();
       $current_time = time();
       $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

       $request->file('image')->move($path , $imageName);
       $newresim = '/img/slider/'.$imageName;

        $slider = new Slider();
        $slider->title = $request->input('title');
        $slider->subtitle = $request->input('subtitle');
        $slider->priority = $request->input('priority');
        $slider->opacity = $request->input('opacity');
        $slider->image = $newresim;
        $slider->link = $request->input('link');
        $slider->status = $request->input('status');
        $slider->save();
        return redirect('/admin/slider');
        Flash::message('Slider başarılı bir şekilde eklendi.','success');
    }

    public function update(Request $request)
    {
        $bak = $request->all();
        // echo "<pre>";
        // print_r($bak);
        // die();
           $image = $request->file('image');
           if ($image) {
             $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

             $destinationPath = public_path('/img/slider/');
             $image->move($destinationPath,$input['imagename']);

             $data['image'] = '/img/slider/'.$input['imagename'];
           }

        Slider::find($bak['id'])->update($bak);
        return redirect()->back();
        Flash::message('Slider başarılı bir şekilde güncellendi.','success');
    }

    public function edit($id)
    {
        $allSlider = Slider::all();
        $slider = Slider::find($id);
        return view('admin.slider.edit',['slider' => $slider, 'allSlider' => $allSlider]);
    }



    public function delete($id) {
        $slider=Slider::find($id);
        $slider->status=0;
        $slider->save();
        return redirect()->back();
    }
}
