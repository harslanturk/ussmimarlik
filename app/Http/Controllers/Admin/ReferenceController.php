<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reference;
use App\ReferenceGallery;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;


class ReferenceController extends Controller
{
    public function index(){
    	$references = Reference::all();

    	return view('admin.reference.index', ['references' => $references]);

    }
    public function create(){
        return view('admin.reference.create');
    }

    public function save(Request $request){
      $data = $request->all();
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/reference';

            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

            $request->file('image')->move($path , $imageName);
            $newresim = '/img/reference/'.$imageName;
        }
        else{
            $newresim="";
        }

        $reference = new Reference();
        $reference->name = $request->input('name');
        $reference->refcontent = $request->input('refcontent');
        $reference->priority = $request->input('priority');
        $reference->image = $newresim;
        $reference->link = $request->input('link');
        $reference->status = $request->input('status');
        $reference->vitrin = $data['vitrin'];
        // echo '<pre>';
        // print_r($reference);
        // die();
        $reference->save();
        Flash::message('Referans başarılı bir şekilde eklendi.','success');
        return redirect('/admin/reference/edit/'.$reference->id);
    }

    public function saveGallery(Request $request){
        $path = base_path() . '/public/img/reference';

        $imageTempName = $request->file('image')->getPathname();
        $current_time = time();
        $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

        $request->file('image')->move($path , $imageName);
        $newresim = '/img/reference/'.$imageName;

        $reference = new ReferenceGallery();
        $reference->reference_id = $request->input('reference_id');
        $reference->image = $newresim;
        $reference->status = 1;
        $reference->save();
        Flash::message('Referans Galeri başarılı bir şekilde eklendi.','success');
        return redirect('/admin/reference/edit/'.$request->input('reference_id'));
    }

    public function update(Request $request)
    {
      $data = $request->all();
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/reference';

            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

            $request->file('image')->move($path , $imageName);
            $newresim = '/img/reference/'.$imageName;
        }
        else{
            $spreference = Reference::where('id', $request->input('id'))->first();
            $newresim = $spreference->image;
        }
        $reference = Reference::find($request->input('id'));
        $reference->name = $request->input('name');
        $reference->refcontent = $request->input('refcontent');
        $reference->priority = $request->input('priority');
        $reference->image = $newresim;
        $reference->link = $request->input('link');
        $reference->status = $request->input('status');
        $reference->vitrin = $data['vitrin'];
        $reference->save();
        return redirect('/admin/reference');
        Flash::message('Referans başarılı bir şekilde güncellendi.','success');
    }

    public function edit($id)
    {
        $allReference = Reference::all();
        $reference = Reference::find($id);
        $referenceGallery = ReferenceGallery::where('reference_id', $reference->id)->get();
        return view('admin.reference.edit',['reference' => $reference, 'allReference' => $allReference, 'referenceGallery' => $referenceGallery]);
    }



    public function delete($id) {
        $reference=Reference::find($id);
        $reference->status=0;
        $reference->save();
        return redirect()->back();
    }

    public function deleteGallery($id) {
        $reference=ReferenceGallery::where('id', $id)->first();
        $reference->status=0;
        $reference->save();
        return redirect()->back();
    }

}
