<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OurClients;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;

class OurClientsController extends Controller
{
    public function index(){

    	$ourclients = OurClients::all();

    	return view('admin.ourclients.index', ['ourclients' => $ourclients]);

    }
    public function create(){

        return view('admin.ourclients.create');
    }

    public function save(Request $request){
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/our-client';

            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

            $request->file('image')->move($path , $imageName);
            $newresim = '/img/our-client/'.$imageName;
        }
        else{
            $newresim="";
        }

        $comment = new OurClients();
        $comment->name = $request->input('name');
        $comment->priority = $request->input('priority');
        $comment->image = $newresim;
        $comment['slug']=str_slug($request->input('name'));
        $comment->position = $request->input('position');
        $comment->comment = $request->input('comment');
        $comment->status = $request->input('status');
        $comment->save();
        return redirect('/admin/our-client');
        Flash::message('Yorum başarılı bir şekilde eklendi.','success');
    }

    public function update(Request $request)
    {
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/our-client';

            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time . "_" . $request->file('image')->getClientOriginalName();

            $request->file('image')->move($path, $imageName);
            $newresim = '/img/our-client/' . $imageName;
        }
        else{
            $spcomment = OurClients::where('id', $request->input('id'))->first();
            $newresim = $spcomment->image;
        }
        $comment = OurClients::find($request->input('id'));
        $comment->name = $request->input('name');
        $comment->priority = $request->input('priority');
        $comment->image = $newresim;
        $comment['slug']=str_slug($request->input('name'));
        $comment->position = $request->input('position');
        $comment->comment = $request->input('comment');
        $comment->status = $request->input('status');
        $comment->save();

        return redirect('/admin/our-client');
        Flash::message('Yorum başarılı bir şekilde güncellendi.','success');
    }

    public function edit($id)
    {
        $allClientComment = OurClients::all();
        $clientcomment = OurClients::find($id);
        return view('admin.ourclients.edit',['clientcomment' => $clientcomment, 'allClientComment' => $allClientComment]);
    }



    public function delete($id)
    {
        $clientcomment=OurClients::find($id);
        $clientcomment->status=0;
        $clientcomment->save();
        return redirect()->back();
    }

}
