<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Service;

class ServicesController extends Controller
{
	public function restorasyon()
  {
    $restorasyon = Page::where('status','1')->where('slug','restorasyon')->get();
    return view('template1.restorasyon',[
      'restorasyon' => $restorasyon
    ]);
	}
  public function insaat()
  {
    $insaat = Page::where('status','1')->where('slug','insaat-ve-taahhut')->get();
    return view('template1.insaat-ve-taahhut',[
      'insaat' => $insaat
    ]);
  }
  public function tadilat()
  {
    $tadilat = Page::where('status','1')->where('slug','tadilat-ve-dekorasyon')->get();
    return view('template1.tadilat-ve-dekorasyon',[
      'tadilat' => $tadilat
    ]);
  }
  public function gayrimenkul()
  {
    $gayrimenkul = Page::where('status','1')->where('slug','gayrimenkul-ve-yatirim-danismanligi')->get();
    return view('template1.gayrimenkul-ve-yatirim-danismanligi',[
      'gayrimenkul' => $gayrimenkul
    ]);
  }
}
