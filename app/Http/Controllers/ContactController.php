<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Blog;
use App\Reference;
use App\Service;
use App\OurClients;
use App\Setting;

class ContactController extends Controller
{
    public function index(){
      $setting = Setting::where('id',1)->first();
    	return view('template1.contact',['setting' => $setting]);
    }
}
