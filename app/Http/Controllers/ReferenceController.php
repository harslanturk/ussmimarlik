<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Reference;

class ReferenceController extends Controller
{
    public function index()
    {
      $referances = Reference::where('status',1)->get();

      // echo '<pre>';
      // print_r($references);
      // die();
    	return view('template1.projelerimiz',[
        'referances' => $referances
      ]);
    }
}
