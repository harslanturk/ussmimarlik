<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reference;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;
use App\Seller;
use App\Page;

class SellerController extends Controller
{
    public function index(){
        $sellers=Seller::where('status','1')->orderBy('priority','asc')->get();
        $pageReference=Page::where('status','1')->where('slug','bayiliklerimiz')->get();
        return view('template0.seller',['sellers'=>$sellers,'pageReference'=>$pageReference]);
    }
}
