<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Havayolu extends Model
{
    protected $table = 'havayolu';
    protected $fillable = [
      'name','status'
    ];
}
