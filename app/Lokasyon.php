<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasyon extends Model
{
    protected $table = 'lokasyon';
    protected $fillable = [
      'name','status'
    ];
}
